<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'passport' => [
        'login_endpoint' => env('PASSPORT_LOGIN_ENDPOINT'),
        'client_id' => env('PASSPORT_CLIENT_ID'),
        'client_secret' => env('PASSPORT_CLIENT_SECRET')
    ],

    'giftpay' => [
        'sandbox' => [
            'api_key' => env('GIFTPAY_API_KEY_SANDBOX'),
            'balance' => env('GIFTPAY_API_BALANCE_SANDBOX'),
            'new_gift_email' => env('GIFTPAY_API_NEW_GIFT_EMAIL_SANDBOX'),
            'new_gift' => env('GIFTPAY_API_NEW_GIFT_SANDBOX'),
            'gift_status' => env('GIFTPAY_API_GIFT_STATUS_SANDBOX')
        ],
        'au' => [
            'api_key' => env('GIFTPAY_API_KEY_AU'),
            'balance' => env('GIFTPAY_API_BALANCE_AU'),
            'new_gift_email' => env('GIFTPAY_API_NEW_GIFT_EMAIL_AU'),
            'new_gift' => env('GIFTPAY_API_NEW_GIFT_AU'),
            'gift_status' => env('GIFTPAY_API_GIFT_STATUS_AU')
        ],
        'us' => [
            'api_key' => env('GIFTPAY_API_KEY_US'),
            'balance' => env('GIFTPAY_API_BALANCE_US'),
            'new_gift_email' => env('GIFTPAY_API_NEW_GIFT_EMAIL_US'),
            'new_gift' => env('GIFTPAY_API_NEW_GIFT_US'),
            'gift_status' => env('GIFTPAY_API_GIFT_STATUS_US')
        ],
        'nz' => [
            'api_key' => env('GIFTPAY_API_KEY_NZ'),
            'balance' => env('GIFTPAY_API_BALANCE_NZ'),
            'new_gift_email' => env('GIFTPAY_API_NEW_GIFT_EMAIL_NZ'),
            'new_gift' => env('GIFTPAY_API_NEW_GIFT_NZ'),
            'gift_status' => env('GIFTPAY_API_GIFT_STATUS_NZ')
        ],
        'uk' => [
            'api_key' => env('GIFTPAY_API_KEY_UK'),
            'balance' => env('GIFTPAY_API_BALANCE_UK'),
            'new_gift_email' => env('GIFTPAY_API_NEW_GIFT_EMAIL_UK'),
            'new_gift' => env('GIFTPAY_API_NEW_GIFT_UK'),
            'gift_status' => env('GIFTPAY_API_GIFT_STATUS_UK')
        ],
    ],

    'apple' => [
        'prod_verify' => env('APPLE_PROD_VERIFY_RECEIPT'),
        'dev_verify' => env('APPLE_DEV_VERIFY_RECEIPT'),
        'shared_secret' => env('APPLE_SHARED_SECRET')
    ],

    'exchange' => [
        'aud_rates' => env('EXCHANGE_RATES')
    ],

    'hash' => [
        'key' => env('HASH_KEY')
    ],

    'myaak' => [
        'endpoint' => env('MYAAK_ENDPOINT')
    ]
];
