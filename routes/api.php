<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware(['auth:api', 'scope:user,admin'])->get('/test', function (Request $request) {

//     $user = $request->user();
//     unset($user->email_verified_at);
//     $response = new stdClass();
//     $response->status = 'success';
//     $response->data = $user;
//     return response()->json($response, 200);
// });


Route::post('/login', 'AuthController@login');
Route::group(['namespace' => 'Auth'], function () {
    Route::post('/scopelogin', 'ApiLoginController@login');
});
Route::post('/register', 'AuthController@register');
Route::get('/regions', 'AuthController@listRegions');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/logout', 'AuthController@logout');

Route::middleware(['auth:api', 'scope:user,admin'])->post('/user/avatar', 'UsersController@addAvatar');
Route::middleware(['auth:api', 'scope:user,admin'])->delete('/user/avatar', 'UsersController@deleteAvatar');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/user/devicetoken', 'UsersController@addDeviceToken');
Route::middleware(['auth:api', 'scope:user,admin'])->delete('/user/devicetoken', 'UsersController@deleteDeviceToken');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/user', 'UsersController@getUserDetails');
Route::middleware(['auth:api', 'scope:user,admin'])->put('/user', 'UsersController@updateUserDetails');

Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/start', 'GameController@startGame');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/challenge', 'GameController@challenge');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/challenge/sent', 'GameController@challengesSent');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/challenge/received', 'GameController@challengesReceived');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/challenge/bets', 'GameController@allowedBets');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/challenge/return', 'GameController@returnChallenge');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/available', 'GameController@nextAvailableKOGames');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/registered', 'GameController@nextAvailableRegisteredKOGames');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/active', 'GameController@activeKOGames');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/completed', 'GameController@completedKOGames');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/register', 'GameController@registerForKOGame');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/unregister', 'GameController@unregisterForKOGame');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/question', 'GameController@getCurrentKOQuestion');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/ko/results', 'GameController@getKOResults');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/progress', 'GameController@getProgress');
// Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/question', 'GameController@getCurrentQuestion');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/game/answer', 'GameController@answerQuestion');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/game/bank', 'GameController@bank');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/game/end', 'GameController@endGame');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/rank', 'GameController@getUserRank');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/game/rank/all', 'GameController@getAllUserRank');
Route::middleware(['auth:api', 'scope:admin'])->get('/game/admin/transactions', 'GameController@getAllTransactions');
Route::middleware(['auth:api', 'scope:admin'])->get('/game/admin/question/{spreadsheetIdentifier}', 'GameController@adminGetQuestion');

Route::middleware(['auth:api', 'scope:user,admin'])->get('/score', 'ScoreController@getCurrentScore');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/score/all', 'ScoreController@getAllScores');

Route::middleware(['auth:api', 'scope:user,admin'])->get('/bank/balance', 'BankController@getBalance');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/credit', 'BankController@credit');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/debit', 'BankController@debit');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/bank/transactions', 'BankController@getTransactions');
Route::middleware(['auth:api', 'scope:admin'])->get('/bank/admin/transactions', 'BankController@getAllTransactions');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/bank/stripe/register', 'BankController@registerStripe');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/stripe/customer', 'BankController@getUserByStripeId');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/stripe/payment_method', 'BankController@createPaymentMethod');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/bank/stripe/payment_methods', 'BankController@getStripePaymentOptions');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/bank/stripe/valid_payment_amounts', 'BankController@getValidStripePaymentAmounts');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/stripe/charge', 'BankController@chargeStripe');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/stripe/subscribe', 'BankController@createSubscription');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/stripe/subscribed', 'BankController@isSubscribed');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/bank/creditkelsfrommyaak', 'BankController@creditKelsFromMyaak');

Route::middleware(['auth:api', 'scope:admin'])->post('/rewards/admin/balance', 'RewardsController@getBalance');
Route::middleware(['auth:api', 'scope:admin'])->get('/rewards/admin/balance/all', 'RewardsController@getAllBalance');
Route::middleware(['auth:api', 'scope:user,admin'])->post('/rewards/purchase', 'RewardsController@purchase');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/rewards', 'RewardsController@getRewards');
Route::middleware(['auth:api', 'scope:user,admin'])->get('/rates', 'RewardsController@getRates');
Route::middleware(['auth:api', 'scope:admin'])->get('/rewards/admin', 'RewardsController@getAllRewards');

Route::get('/features', 'FeaturesController@getFeatures');
Route::middleware(['auth:api', 'scope:admin'])->post('/features/toggle', 'FeaturesController@toggleFeature');


