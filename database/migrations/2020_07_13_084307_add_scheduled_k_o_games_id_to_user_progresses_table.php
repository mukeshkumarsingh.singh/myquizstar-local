<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScheduledKOGamesIdToUserProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_progresses', function (Blueprint $table) {
            $table->bigInteger('scheduled_k_o_games_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_progresses', function (Blueprint $table) {
            $table->dropColumn('scheduled_k_o_games_id');
        });
    }
}
