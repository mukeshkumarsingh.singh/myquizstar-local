<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledKOGamesQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_k_o_games_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('scheduled_k_o_games_id');
            $table->bigInteger('game');
            $table->bigInteger('round');
            $table->bigInteger('questions_id');
            $table->dateTime('asked_at', 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_k_o_games_questions');
    }
}
