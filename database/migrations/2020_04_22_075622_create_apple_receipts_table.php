<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppleReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apple_receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('receipt_type')->nullable();
            $table->bigInteger('adam_id')->nullable();
            $table->bigInteger('app_item_id')->nullable();
            $table->string('bundle_id')->nullable();
            $table->string('application_version')->nullable();
            $table->bigInteger('download_id')->nullable();
            $table->bigInteger('version_external_identifier')->nullable();
            $table->timestamp('receipt_creation_date')->nullable();
            $table->timestamp('request_date')->nullable();
            $table->timestamp('original_purchase_date')->nullable();
            $table->string('original_application_version')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apple_receipts');
    }
}
