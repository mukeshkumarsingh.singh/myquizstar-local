<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledKOGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_k_o_games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('k_o_games_id');
            $table->dateTime('scheduled_for', 0);
            $table->boolean('completed')->default(false);
            $table->string('status');
            $table->bigInteger('winner_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_k_o_games');
    }
}
