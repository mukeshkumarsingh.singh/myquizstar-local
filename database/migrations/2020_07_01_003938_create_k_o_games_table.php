<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKOGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_o_games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('spreadsheet_identifier');
            $table->bigInteger('pot');
            $table->bigInteger('level');
            $table->bigInteger('min_average');
            $table->bigInteger('max_average');
            $table->bigInteger('frequency');
            $table->string('region');
            $table->dateTime('expiry', 0)->nullable();
            $table->dateTime('last_run', 0)->nullable();
            $table->boolean('in_progress');
            $table->boolean('enabled');
            $table->bigInteger('cost_to_play');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_o_games');
    }
}
