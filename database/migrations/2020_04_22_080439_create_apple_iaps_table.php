<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppleIapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apple_iaps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('applereceipt_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('product_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('original_transaction_id')->nullable();
            $table->timestamp('purchase_date')->nullable();
            $table->timestamp('original_purchase_date')->nullable();
            $table->string('is_trial_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apple_iaps');
    }
}
