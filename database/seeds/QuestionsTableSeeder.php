<?php

use App\Category;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{

    public $timestamps = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('questions')->insert([
        //     'question' => 'What colour ball should normally be struck by the cue in snooker?',
        //     'option_a' => 'black',
        //     'option_b' => 'white',
        //     'option_c' => 'mauve',
        //     'option_d' => 'orange',
        //     'answer' => 'B',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Which of the following is another name for Software Piracy?',
        //     'option_a' => 'Pirates',
        //     'option_b' => 'SOPA',
        //     'option_c' => 'Copyright Infringement of Software',
        //     'option_d' => 'SOSA',
        //     'answer' => 'C',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'In what children\'s game are participants chased by someone designated "It"?',
        //     'option_a' => 'Tag',
        //     'option_b' => 'Simon Says',
        //     'option_c' => 'Charades',
        //     'option_d' => 'Hopscotch',
        //     'answer' => 'A',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'On a radio, stations are changed by using what control?',
        //     'option_a' => 'Tuning',
        //     'option_b' => 'Volume',
        //     'option_c' => 'Bass',
        //     'option_d' => 'Treble',
        //     'answer' => 'A',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Which material is most dense?',
        //     'option_a' => 'Silver',
        //     'option_b' => 'Styrofoam',
        //     'option_c' => 'Butter',
        //     'option_d' => 'Gold',
        //     'answer' => 'D',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Which state in the United States is largest by area?',
        //     'option_a' => 'Alaska',
        //     'option_b' => 'California',
        //     'option_c' => 'Texas',
        //     'option_d' => 'Hawaii',
        //     'answer' => 'A',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'What is Aurora Borealis commonly known as?',
        //     'option_a' => 'Fairy Dust',
        //     'option_b' => 'Northern Lights',
        //     'option_c' => 'Book of ages',
        //     'option_d' => 'a Game of Thrones main character',
        //     'answer' => 'B',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Galileo was an Italian astronomer who',
        //     'option_a' => 'developed the telescope',
        //     'option_b' => 'discovered four satellites of Jupiter',
        //     'option_c' => 'discovered that the movement of pendulum produces a regular time measurement',
        //     'option_d' => 'All of the above',
        //     'answer' => 'D',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Exposure to sunlight helps a person improve his health because',
        //     'option_a' => 'the infrared light kills bacteria in the body',
        //     'option_b' => 'resistance power increases',
        //     'option_c' => 'the pigment cells in the skin get stimulated and produce a healthy tan',
        //     'option_d' => 'the ultraviolet rays convert skin oil into Vitamin D',
        //     'answer' => 'D',
        //     'difficulty' => 'medium',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Sir Thomas Fearnley Cup is awarded to',
        //     'option_a' => 'a club or a local sport association for remarkable achievements',
        //     'option_b' => 'amateur athlete, not necessarily an Olympian',
        //     'option_c' => 'National Olympic Committee for outstanding work',
        //     'option_d' => 'None of the above',
        //     'answer' => 'A',
        //     'difficulty' => 'medium',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Oscar Awards were instituted in',
        //     'option_a' => '1968',
        //     'option_b' => '1929',
        //     'option_c' => '1901',
        //     'option_d' => '1965',
        //     'answer' => 'B',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'When did Margaret Thatcher became the first female Prime Minister of Britain?',
        //     'option_a' => '1998',
        //     'option_b' => '1989',
        //     'option_c' => '1979',
        //     'option_d' => '1800',
        //     'answer' => 'C',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'When is the International Workers\' Day?',
        //     'option_a' => '15th April',
        //     'option_b' => '12th December',
        //     'option_c' => '1st May',
        //     'option_d' => '1st August',
        //     'answer' => 'C',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'When did China test their first atomic device?',
        //     'option_a' => '1962',
        //     'option_b' => '1963',
        //     'option_c' => '1964',
        //     'option_d' => '1965',
        //     'answer' => 'C',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'When did Commander Robert Peary discover the North Pole?',
        //     'option_a' => '1904',
        //     'option_b' => '1905',
        //     'option_c' => '1908',
        //     'option_d' => '1909',
        //     'answer' => 'D',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'What is the population density of Kerala?',
        //     'option_a' => '819/sq. km',
        //     'option_b' => '602/sq. km',
        //     'option_c' => '415/sq. km',
        //     'option_d' => '500/sq. km',
        //     'answer' => 'A',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'What is the range of missile \'Akash\'?',
        //     'option_a' => '4 km',
        //     'option_b' => '25 km',
        //     'option_c' => '500 m to 9 km',
        //     'option_d' => '150 km',
        //     'answer' => 'B',
        //     'difficulty' => 'easy',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'In the U.S., if it\'s not Daylight Saving Time, it\'s what?',
        //     'option_a' => 'Borrowed time',
        //     'option_b' => 'Overtime',
        //     'option_c' => 'Standard time',
        //     'option_d' => 'Party time',
        //     'answer' => 'C',
        //     'difficulty' => 'medium',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Which part of the body are you most likely to "stub"?',
        //     'option_a' => 'Toe',
        //     'option_b' => 'Knee',
        //     'option_c' => 'Elbow',
        //     'option_d' => 'Brain',
        //     'answer' => 'A',
        //     'difficulty' => 'medium',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // DB::table('questions')->insert([
        //     'question' => 'Which country is largest by area?',
        //     'option_a' => 'UK',
        //     'option_b' => 'USA',
        //     'option_c' => 'Russia',
        //     'option_d' => 'China',
        //     'answer' => 'C',
        //     'difficulty' => 'hard',
        //     'category_id' => Category::select('id')->inRandomOrder()->first()->id
        // ]);
        // // DB::table('questions')->insert([
        // //     'question' => 'aaa',
        // //     'option_a' => 'bbb',
        // //     'option_b' => 'ccc',
        // //     'option_c' => 'ddd',
        // //     'option_d' => 'eee',
        // //     'answer' => 'fff',
        // //     'difficulty' => 'easy',
        // // ]);
    }
}
