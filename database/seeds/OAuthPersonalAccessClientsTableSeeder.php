<?php

use Illuminate\Database\Seeder;

class OAuthPersonalAccessClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => 1,
            'created_at' => '2020-02-25 07:44:33',
            'updated_at' => '2020-02-25 07:44:33'
        ]);
    }
}
