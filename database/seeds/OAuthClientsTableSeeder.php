<?php

use Illuminate\Database\Seeder;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'name' => 'MyQuizStar Personal Access Client',
            'secret' => 'FE0U8nxAum9i5hpXzr5xSt2OU59uDfKrGxzAEtEX',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
            'created_at' => '2020-02-25 07:44:33',
            'updated_at' => '2020-02-25 07:44:33'
        ]);

        DB::table('oauth_clients')->insert([
            'name' => 'MyQuizStar Password Grant Client',
            'secret' => 'txdrZTHsbIDTNFHU5Y6AI4AhpAmnPFpxe2hN8km1',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
            'created_at' => '2020-02-25 07:44:33',
            'updated_at' => '2020-02-25 07:44:33'
        ]);
    }
}
