<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'primary_category' => 'Sport',
            'secondary_category' => 'AFL'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Sport',
            'secondary_category' => 'Cricket'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Sport',
            'secondary_category' => 'Cycling'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Sport',
            'secondary_category' => 'Soccer'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Music',
            'secondary_category' => 'Classical'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Music',
            'secondary_category' => 'Contemporary'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'Music',
            'secondary_category' => 'Pop'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'History',
            'secondary_category' => 'Ancient'
        ]);
        DB::table('categories')->insert([
            'primary_category' => 'History',
            'secondary_category' => 'Modern'
        ]);
    }
}
