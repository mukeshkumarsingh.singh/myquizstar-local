<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            FeaturesSeeder::class,
            CategoriesTableSeeder::class,
            QuestionsTableSeeder::class,
            OAuthClientsTableSeeder::class,
            OAuthPersonalAccessClientsTableSeeder::class,
        ]);
    }
}
