<?php

use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert([
            'feature' => 'quizstar',
            'display_name' => 'Quiz Star',
            'enabled' => true
        ]);

        DB::table('features')->insert([
            'feature' => 'zero',
            'display_name' => 'Quiz Star Zero',
            'enabled' => true
        ]);

        DB::table('features')->insert([
            'feature' => 'challenge',
            'display_name' => 'Quiz Star Challenge',
            'enabled' => true
        ]);

        DB::table('features')->insert([
            'feature' => 'ko',
            'display_name' => 'Quiz Star Knockout',
            'enabled' => true
        ]);

        DB::table('features')->insert([
            'feature' => 'iap',
            'display_name' => 'Apple In App Payments',
            'enabled' => true
        ]);
    }
}
