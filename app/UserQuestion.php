<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestion extends Model
{
    protected $fillable = [
        'user_id',
        'question_id',
        'user_progresses_id',
        'game',
        'round'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function userProgress() {
        return $this->belongsTo('App\UserProgress');
    }
}
