<?php

namespace App;

use App\ScheduledKOGames;
use Illuminate\Database\Eloquent\Model;

class ScheduledKOGamesPlayers extends Model
{
    
    public function ScheduledKOGames()
    {
        return $this->belongsTo('App\ScheduledKOGames');
    }
}
