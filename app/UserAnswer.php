<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $fillable = [
        'question_id',
        'user_id',
        'given_answer',
        'correct_answer',
        'given_answer',
        'answered_correctly',
        'user_question_id'

    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function userQuestion()
    {
        return $this->belongsTo('App\UserQuestion');
    }
}
