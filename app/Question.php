<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question',
        'option_a',
        'option_b',
        'option_c',
        'option_d',
        'answer',
        'difficulty',
        'category_id',
        'keyword_1',
        'keyword_2',
        'usa',
        'aust',
        'uk',
        'nz',
        'all',
        'spreadsheet_identifier'
    ];


    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function userAnswer()
    {
        return $this->hasMany('App\UserAnswer');
    }
    public function userQuestion()
    {
        return $this->hasMany('App\UserQuestion');
    }
}
