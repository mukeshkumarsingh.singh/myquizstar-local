<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;
use App\Question;

class LoadQuestions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:questions {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load questions from csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $row = 1;
        // echo "Inserting categories...";
        // if (($handle = fopen($this->argument('file'), "r")) !== FALSE) {
        //     while (($data = fgetcsv($handle, 1000, \t)) !== FALSE) {
        //         // print_r($data);
        //         if($row == 1) {
        //             $row++;
        //             continue;
        //         }

        //         $category = Category::where('primary_category', $data[9])->where('secondary_category', $data[10])->first();
        //         if($category) {
        //             echo $data[9] . ' ' . $data[10] . " exists, ignoring.\n";
        //         } else {
        //             echo "inserting " . $data[9] . ' ' . $data[10] . "\n";
        //             $newCat = new Category();
        //             $newCat->primary_category = $data[9];
        //             $newCat->secondary_category = $data[10];
        //             $newCat->save();
        //         }
        //     }
        //     fclose($handle);
        // }

        $row = 1;
        echo "Inserting questions...";
        if (($handle = fopen($this->argument('file'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if($row == 1) {
                    $row++;
                    continue;
                }

                $category = Category::where('primary_category', $data[9])->where('secondary_category', $data[10])->first();
                if(!$category) {
                    echo "inserting " . $data[9] . ' ' . $data[10] . "\n";
                    $category = new Category();
                    $category->primary_category = $data[9];
                    $category->secondary_category = $data[10];
                    $category->save();
                }

                $spreadsheetIdentifier = (int) filter_var($data[0], FILTER_SANITIZE_NUMBER_INT);
                if($spreadsheetIdentifier % 1000 === 0) {
                    echo $spreadsheetIdentifier;
                }

                $question = Question::where('spreadsheet_identifier', $spreadsheetIdentifier)->first();

                if(!$question) {
                    echo "inserting " . $data[0] . " " . $data[1] . "\n";
                    $question = new Question();
                    $question->spreadsheet_identifier = $spreadsheetIdentifier;
                    $question->question = $data[1];
                    $question->keyword_1 = $data[2];
                    $question->keyword_2 = $data[3];
                    $answers = Array();
                    array_push($answers, $data[4]);
                    array_push($answers, $data[5]);
                    array_push($answers, $data[6]);
                    array_push($answers, $data[7]);
                    shuffle($answers);
                    // var_dump($answers);
                    // echo 'Correct: '.$data[4] . "\n";
                    $question->option_a = $answers[0];
                    if($answers[0] == $data[4]) {
                        $question->answer = 'A';
                    }
                    $question->option_b = $answers[1];
                    if($answers[1] == $data[4]) {
                        $question->answer = 'B';
                    }
                    $question->option_c = $answers[2];
                    if($answers[2] == $data[4]) {
                        $question->answer = 'C';
                    }
                    $question->option_d = $answers[3];
                    if($answers[3] == $data[4]) {
                        $question->answer = 'D';
                    }
                    $question->difficulty = $data[8];
                    $question->siri = $data[11];
                    $question->author = $data[12];
                    $question->date = '2020-05-05';
                    if($data[14] == 'USA') {
                        $question->usa = true;
                    } else {
                        $question->usa = false;
                    }
                    if($data[15] == 'AUST') {
                        $question->aust = true;
                    } else {
                        $question->aust = false;
                    }
                    if($data[16] == 'UK') {
                        $question->uk = true;
                    } else {
                        $question->uk = false;
                    }
                    if($data[17] == 'NZ') {
                        $question->nz = true;
                    } else {
                        $question->nz = false;
                    }
                    if($data[18] == 'ALL') {
                        $question->all = true;
                    } else {
                        $question->all = false;
                    }
                    // $category = Category::where('primary_category', $data[9])->where('secondary_category', $data[10])->first();
                    $question->category_id = $category->id;
                    $question->save();
                    // $newCat = new Category();
                    // $newCat->primary_category = $data[9];
                    // $newCat->secondary_category = $data[10];
                    // $newCat->save();
                } else {
                    echo '.';
                }
            }
            fclose($handle);
        }
    }
}
