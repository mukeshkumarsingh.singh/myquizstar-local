<?php

namespace App\Console\Commands;

use App\KOGame;
use App\ScheduledKOGames;
use App\ScheduledKOGamesPlayers;
use App\Balance;
use App\Question;
use App\ScheduledKOGamesQuestions;
use App\Transaction;
use App\UserProgress;
use App\UserQuestion;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ScheduleKOGames extends Command
{
    const CREDIT = 'CREDIT';
    const DEBIT = 'DEBIT';

    const WAITING = 'waiting';
    const IN_PROGRESS = 'in_progress';
    const COMPLETE = 'complete';
    const EXPIRED = 'expired';

    const EASY = 'Easy';
    const MEDIUM = 'Medium';
    const HARD = 'Hard';
    const DIFFICULT = 'Difficult';
    const VERY_DIFFICULT = 'Very Difficult';

    protected $progress = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:ko';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled KO Games, designed to run every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->populateRuleset();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games = KOGame::where('enabled', true)->get();
        // $scheduledGames = ScheduleKOGames::where('completed', false);
        foreach ($games as $game) {
            // check if we have a schedule to run shortly
            $scheduledGame = ScheduledKOGames::where('k_o_games_id', $game->id)->where('completed', false)->first();
            if (!$scheduledGame) {
                $scheduledGame = $this->scheduleNewGame($game->id, Carbon::now()->addMinutes($game->frequency));
            } else {
                // if (!$scheduledGame->completed) {
                //check if we should timeout
                // var_dump(Carbon::parse($scheduledGame->scheduled_for)->addMinutes(18)->format('Y-m-d H:i:s') . ' vs ' . Carbon::now()->format('Y-m-d H:i:s'));
                if (Carbon::parse($scheduledGame->scheduled_for) < Carbon::now() && Carbon::parse($scheduledGame->scheduled_for)->addMinutes(18) > Carbon::now()) {
                    $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('state', '<>', $this::COMPLETE)->get();
                    if (count($registeredPlayers) < 2) {
                        $this->returnFunds($scheduledGame, $registeredPlayers);
                        $scheduledGame->status = $this::EXPIRED;
                        $scheduledGame->winner_id = 0;
                        $scheduledGame->completed = true;
                        $scheduledGame->save();
                    } else {
                        if ($scheduledGame->status != $this::IN_PROGRESS) {
                            //start the game
                            $this->startGame($scheduledGame->id);
                            $scheduledGame->status = $this::IN_PROGRESS;
                            $scheduledGame->save();
                            $this->startPlayers($scheduledGame, $registeredPlayers);
                        }
                    }
                } else if (Carbon::parse($scheduledGame->scheduled_for)->addMinutes(18) < Carbon::now()) {
                    //this game has expired, end it

                    $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('state', '<>', $this::COMPLETE)->get();
                    $this->returnFunds($scheduledGame, $registeredPlayers);
                    $scheduledGame->status = $this::EXPIRED;
                    $scheduledGame->winner_id = 0;
                    $scheduledGame->completed = true;
                    $scheduledGame->save();

                    //check how many players are playing and mark it as complete if finished
                    // $players = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where();
                }
                // }
            }
        }
    }

    private function startPlayers($scheduledGame, $registeredPlayers)
    {
        foreach ($registeredPlayers as $registeredPlayer) {
            //get the first question of the game
            $registeredPlayer->state = $this::IN_PROGRESS;
            $registeredPlayer->save();
            $firstQuestion = ScheduledKOGamesQuestions::where('scheduled_k_o_games_id', $scheduledGame->id)->where('game', 0)->where('round', 0)->first();
            // mark any existing games as complete
            $userProgresses = UserProgress::where('user_id', $registeredPlayer->user_id)->where('status', self::IN_PROGRESS)->get();
            foreach ($userProgresses as $progress) {
                $progress->status = $this::COMPLETE;
                $progress->save();
            }
            $userProgress = new UserProgress();
            $userProgress->user_id = $registeredPlayer->user_id;
            $userProgress->game = 0;
            $userProgress->round = 0;
            $userProgress->status = $this::IN_PROGRESS;
            $userProgress->question_id = $firstQuestion->questions_id;
            $userProgress->type = "My Quiz Star KO";
            $userProgress->scheduled_k_o_games_id = $scheduledGame->id;
            $userProgress->save();

            $userQuestion = new UserQuestion();
            $userQuestion->user_id = $registeredPlayer->user_id;
            $userQuestion->question_id = $firstQuestion->questions_id;
            $userQuestion->game = $userProgress->game;
            $userQuestion->round = $userProgress->round;
            $userQuestion->user_progresses_id = $userProgress->id;
            $userQuestion->save();
        }
    }

    private function startGame($gameId)
    {
        // preload all the questions
        $game = 0;
        $round = 0;
        for ($i = 0; $i <= 100; $i++) {
            //get a question that is appropriate for the this game/round
            $question = Question::where('difficulty', $this->progress[$game][$round])->inRandomOrder()->first();;
            // var_dump($question->id);
            $scheduledQuestion = new ScheduledKOGamesQuestions();
            $scheduledQuestion->scheduled_k_o_games_id = $gameId;
            $scheduledQuestion->game = $game;
            $scheduledQuestion->round = $round;
            $scheduledQuestion->questions_id = $question->id;
            $scheduledQuestion->save();
            $round++;
            if ($round == 9 && $game == 9) {
                return;
            }
            if ($round == 10) {
                $round = 0;
                $game++;
            }
        }
    }

    private function scheduleNewGame($gameId, $when)
    {
        $scheduledGame = new ScheduledKOGames();
        $scheduledGame->k_o_games_id = $gameId;
        $scheduledGame->scheduled_for = $when;
        $scheduledGame->status = $this::WAITING;
        $scheduledGame->completed = false;
        $scheduledGame->save();
        return $scheduledGame;
    }

    private function returnFunds($scheduledGame, $players)
    {
        foreach ($players as $player) {
            $player->state = $this::COMPLETE;
            $player->save();

            // kill and progresses
            $userProgress = UserProgress::where('scheduled_k_o_games_id', $player->scheduled_k_o_games_id)->where('user_id', $player->user_id)->first();
            $userProgress->status = $this::COMPLETE;
            $userProgress->save();

            $balance = Balance::where('user_id', $player->user_id)->first();
            $balance->balance = $balance->balance + 25;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = $player->user_id;
            $transaction->amount = 25;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::CREDIT;
            $transaction->category = 'KO';
            $transaction->description = 'KO Game returned.';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();

            $balance->balance = $balance->balance + $scheduledGame->KOGames->pot;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = $player->user_id;
            $transaction->amount = $scheduledGame->KOGames->pot;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::CREDIT;
            $transaction->category = 'KO';
            $transaction->description = 'KO Game ' . $scheduledGame->KOGames->id . ' POT holding returned.';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();
        }
    }

    private function getUniqueTransactionReference()
    {
        while (true) {
            $reference = md5(uniqid(rand(), true));
            $transaction = Transaction::where('reference', $reference)->get();
            if ($transaction->isEmpty()) {
                return md5(uniqid(rand(), true));
            }
        }
    }

    private function populateRuleset()
    {
        $this->progress[0][0] = self::EASY;
        $this->progress[0][1] = self::EASY;
        $this->progress[0][2] = self::EASY;
        $this->progress[0][3] = self::EASY;
        $this->progress[0][4] = self::EASY;
        $this->progress[0][5] = self::EASY;
        $this->progress[0][6] = self::EASY;
        $this->progress[0][7] = self::EASY;
        $this->progress[0][8] = self::MEDIUM;
        $this->progress[0][9] = self::MEDIUM;

        $this->progress[1][0] = self::EASY;
        $this->progress[1][1] = self::EASY;
        $this->progress[1][2] = self::EASY;
        $this->progress[1][3] = self::EASY;
        $this->progress[1][4] = self::EASY;
        $this->progress[1][5] = self::EASY;
        $this->progress[1][6] = self::EASY;
        $this->progress[1][7] = self::MEDIUM;
        $this->progress[1][8] = self::MEDIUM;
        $this->progress[1][9] = self::HARD;

        $this->progress[2][0] = self::EASY;
        $this->progress[2][1] = self::EASY;
        $this->progress[2][2] = self::EASY;
        $this->progress[2][3] = self::EASY;
        $this->progress[2][4] = self::EASY;
        $this->progress[2][5] = self::EASY;
        $this->progress[2][6] = self::MEDIUM;
        $this->progress[2][7] = self::MEDIUM;
        $this->progress[2][8] = self::HARD;
        $this->progress[2][9] = self::HARD;

        $this->progress[3][0] = self::EASY;
        $this->progress[3][1] = self::EASY;
        $this->progress[3][2] = self::EASY;
        $this->progress[3][3] = self::EASY;
        $this->progress[3][4] = self::EASY;
        $this->progress[3][5] = self::MEDIUM;
        $this->progress[3][6] = self::MEDIUM;
        $this->progress[3][7] = self::HARD;
        $this->progress[3][8] = self::HARD;
        $this->progress[3][9] = self::HARD;

        $this->progress[4][0] = self::EASY;
        $this->progress[4][1] = self::EASY;
        $this->progress[4][2] = self::MEDIUM;
        $this->progress[4][3] = self::MEDIUM;
        $this->progress[4][4] = self::MEDIUM;
        $this->progress[4][5] = self::HARD;
        $this->progress[4][6] = self::HARD;
        $this->progress[4][7] = self::HARD;
        $this->progress[4][8] = self::HARD;
        $this->progress[4][9] = self::DIFFICULT;

        $this->progress[5][0] = self::EASY;
        $this->progress[5][1] = self::MEDIUM;
        $this->progress[5][2] = self::MEDIUM;
        $this->progress[5][3] = self::MEDIUM;
        $this->progress[5][4] = self::HARD;
        $this->progress[5][5] = self::HARD;
        $this->progress[5][6] = self::HARD;
        $this->progress[5][7] = self::HARD;
        $this->progress[5][8] = self::DIFFICULT;
        $this->progress[5][9] = self::DIFFICULT;

        $this->progress[6][0] = self::MEDIUM;
        $this->progress[6][1] = self::MEDIUM;
        $this->progress[6][2] = self::HARD;
        $this->progress[6][3] = self::HARD;
        $this->progress[6][4] = self::HARD;
        $this->progress[6][5] = self::HARD;
        $this->progress[6][6] = self::DIFFICULT;
        $this->progress[6][7] = self::DIFFICULT;
        $this->progress[6][8] = self::DIFFICULT;
        $this->progress[6][9] = self::VERY_DIFFICULT;

        $this->progress[7][0] = self::MEDIUM;
        $this->progress[7][1] = self::MEDIUM;
        $this->progress[7][2] = self::HARD;
        $this->progress[7][3] = self::HARD;
        $this->progress[7][4] = self::HARD;
        $this->progress[7][5] = self::HARD;
        $this->progress[7][6] = self::DIFFICULT;
        $this->progress[7][7] = self::DIFFICULT;
        $this->progress[7][8] = self::DIFFICULT;
        $this->progress[7][9] = self::VERY_DIFFICULT;

        $this->progress[8][0] = self::MEDIUM;
        $this->progress[8][1] = self::HARD;
        $this->progress[8][2] = self::HARD;
        $this->progress[8][3] = self::DIFFICULT;
        $this->progress[8][4] = self::DIFFICULT;
        $this->progress[8][5] = self::DIFFICULT;
        $this->progress[8][6] = self::DIFFICULT;
        $this->progress[8][7] = self::VERY_DIFFICULT;
        $this->progress[8][8] = self::VERY_DIFFICULT;
        $this->progress[8][9] = self::VERY_DIFFICULT;

        $this->progress[9][0] = self::DIFFICULT;
        $this->progress[9][1] = self::DIFFICULT;
        $this->progress[9][2] = self::DIFFICULT;
        $this->progress[9][3] = self::DIFFICULT;
        $this->progress[9][4] = self::DIFFICULT;
        $this->progress[9][5] = self::VERY_DIFFICULT;
        $this->progress[9][6] = self::VERY_DIFFICULT;
        $this->progress[9][7] = self::VERY_DIFFICULT;
        $this->progress[9][8] = self::VERY_DIFFICULT;
        $this->progress[9][9] = self::VERY_DIFFICULT;
    }
}
