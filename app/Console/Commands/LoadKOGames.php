<?php

namespace App\Console\Commands;

use App\KOGame;
use Illuminate\Console\Command;

class LoadKOGames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:kogames {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loads KO games from the given CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $row = 1;
        echo "Inserting KO Games...";
        if (($handle = fopen($this->argument('file'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if($row == 1) {
                    $row++;
                    continue;
                }
                //check if game exists
                $game = KOGame::where('spreadsheet_identifier', $data[0])->first();
                $this->createGame($data, $game);
            }
        }
    }

    private function createGame($data, $game) {
        if(!$game) {
            //add
            $game = new KOGame();
            echo('Adding ' . $data[0] . '...' . "\n");
            $game->in_progress = false;
            $game->enabled = true;
        } else {
            echo('Updating ' . $data[0] . '...' . "\n");
        }
        //update
        $game->spreadsheet_identifier = $data[0];
        $game->pot = $data[1]*100;
        $game->level = $data[2];
        $average = explode(' to ', $data[3]);
        $game->min_average = floor($average[0])*100;
        $game->max_average = floor($average[1])*100;
        $game->frequency = $data[4];
        $game->region = $data[5];
        $expiry = null;
        if($data[6] !== '') {
            $expiry = $data[6];
        }
        $game->expiry = $expiry;
        $game->cost_to_play = $data[7]*100;

        $game->save();
    }
}
