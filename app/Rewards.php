<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rewards extends Model
{
    protected $fillable = [
        'user_id',
        'amount',
        'expiry',
        'gift_id',
        'transaction_id',
        'message',
        'status_code',
        'conversion_rate',
        'native_amount'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
}
