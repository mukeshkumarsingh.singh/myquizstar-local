<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppleReceipt extends Model
{
    protected $fillable = [
        'receipt_type',
        'adam_id',
        'app_item_id',
        'bundle_id',
        'application_version',
        'download_id',
        'version_external_identifier',
        'receipt_creation_date',
        'request_date',
        'original_purchase_date',
        'original_application_version',
    ];


    public function appleIap()
    {
        return $this->hasMany('App\AppleIap');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
