<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppleIap extends Model
{
    protected $fillable = [
        'appereceipt_id',
        'quantity',
        'product_id',
        'transaction_id',
        'original_transaction_id',
        'purchase_date',
        'original_purchase_date',
        'is_trial_period',
    ];


    public function category()
    {
        return $this->hasMany('App\AppleIap');
    }
}
