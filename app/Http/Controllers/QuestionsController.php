<?php

// namespace App\Http\Controllers;

// use App\Category;
// use App\Question;
// use App\UserAnswer;
// use Illuminate\Http\Request;
// use Auth;

// class QuestionsController extends Controller
// {
//     const EASY = 'easy';
//     const MEDIUM = 'medium';
//     const HARD = 'hard';
//     const DIFFICULT = 'difficult';
//     const VERY_DIFFICULT = 'very_difficult';

//     public function getQuestion(Request $request)
//     {
//         $difficulty = strtolower($request->difficulty);
//         if ($difficulty != self::EASY && $difficulty != self::MEDIUM && $difficulty != self::HARD) {
//             return response()->json([
//                 'status' => 'error',
//                 'message' => 'Invalid request. Difficulty must be easy, medium or hard.'
//             ], 400);
//         }

//         $primaryCategory = strtolower($request->category);
//         $categories = Category::where('primary_category', $primaryCategory)->get();
//         if ($categories->isEmpty()) {
//             return response()->json([
//                 'status' => 'error',
//                 'message' => 'Invalid request. Given category \'' . $primaryCategory . '\' is invalid.'
//             ], 400);
//         } else {
//             $secondaryCategory = strtolower($request->subCategory);
//             $secondaryCategoryExists = Category::where('primary_category', $primaryCategory)->where('secondary_category', $secondaryCategory)->first();
//             if (!$secondaryCategoryExists) {
//                 return response()->json([
//                     'status' => 'error',
//                     'message' => 'Invalid request. Given sub category \'' . $secondaryCategory . '\' is invalid.'
//                 ], 400);
//             }
//         }

//         $category = Category::where('primary_category', $primaryCategory)->where('secondary_category', $secondaryCategory)->first();
//         //check if the user has answered any questions already
//         $answeredQuestions = UserAnswer::where('user_id', Auth::user()->id);
//         $answeredQuestionIds = [];
//         if ($answeredQuestions) {
//             $answeredQuestionIds = $answeredQuestions->pluck('question_id');
//         }
//         $query = Question::where('difficulty', $difficulty)
//             ->where('category_id', $category->id)
//             ->whereNotIn('id', $answeredQuestionIds)
//             ->inRandomOrder()->first();
//         if (!$query) {
//             return response()->json([
//                 'status' => 'success',
//                 'message' => 'No questions available.',
//                 'data' => []
//             ], 200);
//         }
//         return response()->json([
//             'status' => 'success',
//             'data' => [
//                 'id' => $query->id,
//                 'question' => $query->question,
//                 'a' => $query->option_a,
//                 'b' => $query->option_b,
//                 'c' => $query->option_c,
//                 'd' => $query->option_d,
//                 'difficulty' => $query->difficulty,
//                 'primary_category' => $category->primary_category,
//                 'secondary_category' => $category->secondary_category,
//             ]
//         ], 200);
//     }

//     public function answerQuestion(Request $request)
//     {
//         $questionId = strtolower($request->questionId);
//         $question = Question::find($questionId);
//         if (!$question) {
//             return response()->json([
//                 'status' => 'error',
//                 'message' => 'Invalid request. Invalid question ID'
//             ], 400);
//         }
//         $existingAnswer = UserAnswer::where('question_id', $questionId)->where('user_id', Auth::user()->id)->first();
//         if ($existingAnswer) {
//             return response()->json([
//                 'status' => 'error',
//                 'message' => 'Invalid request. This question has already been answered and cannot be answered again.'
//             ], 400);
//         }

//         $answer = strtolower($request->answer);
//         if (!$answer) {
//             return response()->json([
//                 'status' => 'error',
//                 'message' => 'Invalid request. You must provide an answer.'
//             ], 400);
//         }
//         $userAnswer = new UserAnswer();
//         $userAnswer->question_id = $questionId;
//         $userAnswer->user_id = Auth::user()->id;
//         $userAnswer->given_answer = $answer;
//         $userAnswer->correct_answer = $question->answer;
//         if (strtolower($question->answer) == $answer) {
//             //correct
//             $userAnswer->answered_correctly = true;
//             $userAnswer->save();
//             return response()->json([
//                 'status' => 'success',
//                 'data' => [
//                     'id' => $questionId,
//                     'correct' => true,
//                     'correct_answer' => $question->answer
//                 ]
//             ], 200);
//         } else {
//             //incorrect
//             $userAnswer->answered_correctly = false;
//             $userAnswer->save();
//             return response()->json([
//                 'status' => 'success',
//                 'data' => [
//                     'id' => $questionId,
//                     'correct' => false,
//                     'correct_answer' => $question->answer
//                 ]
//             ], 200);
//         }
//     }
// }
