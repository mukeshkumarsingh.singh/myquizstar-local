<?php

namespace App\Http\Controllers;

use stdClass;
use Illuminate\Http\Request;
use App\Balance;
use App\Transaction;
use App\Rewards;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Carbon;

class RewardsController extends Controller
{
    protected $giftpay;
    // protected $apiKey;
    // protected $apiBalance;
    // protected $apiGiftStatus;
    // protected $apiNewGift;
    // protected $apiNewGiftByEmail;

    const CREDIT = 'CREDIT';
    const DEBIT = 'DEBIT';

    public function __construct()
    {

        $this->giftpay['sandbox'] = new \stdClass();
        $this->giftpay['sandbox']->apiKey = config('services.giftpay.sandbox.api_key');
        $this->giftpay['sandbox']->apiBalance = config('services.giftpay.sandbox.balance');
        $this->giftpay['sandbox']->apiGiftStatus = config('services.giftpay.sandbox.gift_status');
        $this->giftpay['sandbox']->apiNewGift = config('services.giftpay.sandbox.new_gift');
        $this->giftpay['sandbox']->apiNewGiftByEmail = config('services.giftpay.sandbox.new_gift_email');

        // $this->apiKey = config('services.giftpay.api_key');
        // $this->apiBalance = config('services.giftpay.balance');
        // $this->apiGiftStatus = config('services.giftpay.gift_status');
        // $this->apiNewGift = config('services.giftpay.new_gift');
        // $this->apiNewGiftByEmail = config('services.giftpay.new_gift_email');

        $this->giftpay['au'] = new \stdClass();
        $this->giftpay['au']->apiKey = config('services.giftpay.au.api_key');
        $this->giftpay['au']->apiBalance = config('services.giftpay.au.balance');
        $this->giftpay['au']->apiGiftStatus = config('services.giftpay.au.gift_status');
        $this->giftpay['au']->apiNewGift = config('services.giftpay.au.new_gift');
        $this->giftpay['au']->apiNewGiftByEmail = config('services.giftpay.au.new_gift_email');

        $this->giftpay['us'] = new \stdClass();
        $this->giftpay['us']->apiKey = config('services.giftpay.us.api_key');
        $this->giftpay['us']->apiBalance = config('services.giftpay.us.balance');
        $this->giftpay['us']->apiGiftStatus = config('services.giftpay.us.gift_status');
        $this->giftpay['us']->apiNewGift = config('services.giftpay.us.new_gift');
        $this->giftpay['us']->apiNewGiftByEmail = config('services.giftpay.us.new_gift_email');

        $this->giftpay['nz'] = new \stdClass();
        $this->giftpay['nz']->apiKey = config('services.giftpay.nz.api_key');
        $this->giftpay['nz']->apiBalance = config('services.giftpay.nz.balance');
        $this->giftpay['nz']->apiGiftStatus = config('services.giftpay.nz.gift_status');
        $this->giftpay['nz']->apiNewGift = config('services.giftpay.nz.new_gift');
        $this->giftpay['nz']->apiNewGiftByEmail = config('services.giftpay.nz.new_gift_email');

        $this->giftpay['uk'] = new \stdClass();
        $this->giftpay['uk']->apiKey = config('services.giftpay.uk.api_key');
        $this->giftpay['uk']->apiBalance = config('services.giftpay.uk.balance');
        $this->giftpay['uk']->apiGiftStatus = config('services.giftpay.uk.gift_status');
        $this->giftpay['uk']->apiNewGift = config('services.giftpay.uk.new_gift');
        $this->giftpay['uk']->apiNewGiftByEmail = config('services.giftpay.uk.new_gift_email');
    }

    public function getBalance(Request $request)
    {
        $region = 'sandbox';
        if ($request->region) {
            $region = $request->region;
        }

        $data = $this->getRawBalance($region);
        if(isset($data->status) && $data->status == 'error') {
            return response()->json($data, $data->statusCode);
        } else {
            return response()->json([
                'status' => 'success',
                'data' => $data
            ], 200);
        }
    }

    public function getAllBalance()
    {
        $regions = ['sandbox', 'au', 'us', 'uk', 'nz'];
        $result = new \stdClass();

        foreach($regions as $region) {
            $response = $this->getRawBalance($region);
            $result->$region = new \stdClass();
            $result->$region->Balance = $response->Balance;
            $result->$region->StatusCode = $response->StatusCode;
            $result->$region->Region = $response->Region;
        }

        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    public function getRewards()
    {
        $rewards = Rewards::where('user_id', Auth::user()->id)->where('status_code', 1)->orderBy('id', 'DESC')->get();
        if (!$rewards) {
            // no rewards
            return response()->json([
                'status' => 'success',
                'data' => []
            ], 200);
        }
        $result = array();
        foreach ($rewards as $reward) {
            $data = array();

            $data['amount'] = $reward->amount;
            $data['expiry'] = $reward->expiry;
            $data['date'] = $reward->created_at;
            $data['gift_id'] = $reward->gift_id;
            $data['region'] = $reward->region;
            // get the status of each gift
            $params = ['query' => [
                'key' => $this->giftpay[$reward->region]->apiKey,
                'giftid' => $reward->gift_id,
                'email' => Auth::user()->email
            ]];
            $http = new \GuzzleHttp\Client;
            try {
                $response = $http->request('GET', $this->giftpay[$reward->region]->apiGiftStatus, $params);
                // dd(json_decode($response->getBody()));
                $data['status'] = json_decode($response->getBody());
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $data['status'] = '';
            }

            array_push($result, $data);
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    public function getAllRewards()
    {
        $rewards = Rewards::with('transaction')->with('user')->orderBy('id', 'DESC')->get();
        if (!$rewards) {
            // no rewards
            return response()->json([
                'status' => 'success',
                'data' => []
            ], 200);
        }

        $result = array();
        foreach ($rewards as $reward) {
            $data = array();

            $data['amount'] = $reward->amount;
            $data['expiry'] = $reward->expiry;
            $data['date'] = $reward->created_at;
            $data['gift_id'] = $reward->gift_id;
            $data['transaction'] = $reward->transaction;
            $data['user'] = $reward->user;
            $data['region'] = $reward->region;

            // get the status of each gift
            $params = ['query' => [
                'key' => $this->giftpay[$reward->region]->apiKey,
                'giftid' => $reward->gift_id
            ]];
            $http = new \GuzzleHttp\Client;
            try {
                $response = $http->request('GET', $this->giftpay[$reward->region]->apiGiftStatus, $params);
                // dd(json_decode($response->getBody()));
                $data['status'] = json_decode($response->getBody());
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                $data['status'] = '';
            }

            array_push($result, $data);
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    public function purchase(Request $request)
    {
        $amount = $request->amount;
        //ensure amount is between 5 and 1000 and multiple of 5
        if ($amount / 100 < 5) {
            return response()->json([
                'status' => 'error',
                'message' => 'Purchase must be at least $5'
            ], 400);
        }
        if ($amount / 100 > 50) {
            return response()->json([
                'status' => 'error',
                'message' => 'Purchase must be less than $50'
            ], 400);
        }
        if ($amount / 100 % 5 !== 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Purchase must be a multiple of $5'
            ], 400);
        }

        $region = $request->region;
        if(!isset($region)) {
            return response()->json([
                'status' => 'error',
                'message' => 'No region specified'
            ], 400);
        }
        if($region != 'sandbox' && $region != 'au' && $region != 'nz' && $region != 'uk' && $region != 'us') {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid region provided'
            ], 400);
        }
        $rateLink = [];
        $rateLink['sandbox'] = 'AUD';
        $rateLink['au'] = 'AUD';
        $rateLink['us'] = 'USD';
        $rateLink['uk'] = 'GBP';
        $rateLink['nz'] = 'NZD';

        $rates = (array) $this->getRawRates();

        $conversionRate = $rates[$rateLink[$region]];

        $nativeAmount = ceil($amount / $conversionRate);


        //check if we have sufficient funds
        $balance = $this->balance();
        if ($nativeAmount > $balance->balance) {
            return response()->json([
                'status' => 'error',
                'message' => 'Insufficient balance to complete purchase.'
            ], 400);
        }

        // //ensure we can only withdraw $50 a day
        $lastDayRewardAmounts = Rewards::where('created_at', '>=', Carbon::now()->subDay())->where('user_id', Auth::user()->id)->get();

        if ((collect($lastDayRewardAmounts)->sum('amount') + $amount) >= 5000) {
            return response()->json([
                'status' => 'error',
                'message' => 'You can only widthdraw up to $50 in gift cards each day. You may only widthraw another $' . (50 - (collect($lastDayRewardAmounts)->sum('amount') / 100)) . '.'
            ], 200);
        }

        $from = 'Damnn Bank';
        if (isset($request->from) && $request->from != "") {
            $from = $request->from;
        }
        // add the transaction first so we can record the ID
        $transactionReference = $this->getUniqueTransactionReference();
        try {
            $balance->balance = $balance->balance - $nativeAmount;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = $nativeAmount;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::DEBIT;
            $transaction->category = 'Gift Card';
            $transaction->description = 'Purchase Gift Card';
            $transaction->reference = $transactionReference;
            $transaction->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => "Unfortunately, we're unable to complete your purchase at this time. Please contact support."
            ], 500);
        }
        $params = [
            'query' => [
                'key' => $this->giftpay[$region]->apiKey,
                'to' => Auth::user()->email,
                'value' => $amount / 100,
                'clientref' => $transactionReference,
                'from' => $from
            ]
        ];
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->request('GET', $this->giftpay[$region]->apiNewGiftByEmail, $params);
            $responseBody = json_decode($response->getBody());
            if ($responseBody->StatusCode === 1) {
                $reward = new Rewards();
                $reward->amount = $amount;
                $reward->native_amount = $nativeAmount;
                $reward->conversion_rate = $conversionRate;
                $reward->region = $region;
                $reward->user_id = Auth::user()->id;
                $reward->expiry = $responseBody->Expiry;
                $reward->gift_id = $responseBody->GiftID;
                $reward->transaction_id = $transaction->id;
                $reward->message = $responseBody->Message;
                $reward->status_code = $responseBody->StatusCode;
                $reward->save();
                return response()->json([
                    'status' => 'success',
                    'data' => $responseBody
                ], 200);
            } else {
                throw new \Exception($this->getGiftPayMessage($responseBody->StatusCode));
            }
        } catch (\Exception $e) {
            // reverse the transactions
            $balance->balance = $balance->balance + $nativeAmount;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = $nativeAmount;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::CREDIT;
            $transaction->category = 'Gift Card Refund';
            $transaction->description = 'Unable to complete transaction';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();

            $err = new stdClass();
            $err->message = $e->getMessage();
            $err->status = 'error';
            return response()->json($err, 500);
        }
    }

    public function getRates() {
        $rawRates = (array) $this->getRawRates();

        $rates = new \stdClass();
        $rates->sandbox = 1;
        $rates->au = $rawRates['AUD'];
        $rates->us = $rawRates['USD'];
        $rates->nz = $rawRates['NZD'];
        $rates->uk = $rawRates['GBP'];

        return response()->json([
            'status' => 'success',
            'data' => $rates
        ], 200);
    }

    private function getGiftPayMessage($statusCode)
    {
        switch ($statusCode) {
            case -1:
                return 'Authorisation failed';
            case -2:
                return 'Reserved';
            case -3:
                return 'Reserved';
            case -4:
                return 'Program gift credit exhausted';
            case -5:
                return 'Program daily limit would be exceeded';
            case -6:
                return 'Duplicate clientref while value and/or recipient do not match';
            case -7:
                return 'The gift ID is invalid, or the gift ID does not exist, or the gift has already been cancelled.';
            case -8:
                return 'The email address does not match the gift ID.';
            case -9:
                return 'Invalid parameter';
            case -10:
                return 'The API key type is different to the API call type';
            case -99:
                return 'Internal server error';
            default:
                return 'Unknown';
        }
    }

    private function balance()
    {
        $balance = Balance::where('user_id', Auth::user()->id)->first();
        if (!$balance) {
            $balance = new Balance();
            $balance->balance = 0;
            $balance->kels = 100;
            $balance->user_id = Auth::user()->id;
            $balance->save();
        }
        return $balance;
    }

    private function getUniqueTransactionReference()
    {
        while (true) {
            $reference = md5(uniqid(rand(), true));
            $transaction = Transaction::where('reference', $reference)->get();
            if ($transaction->isEmpty()) {
                return md5(uniqid(rand(), true));
            }
        }
    }

    private function getRawBalance($region) {
        $params = ['query' => [
            'key' => $this->giftpay[$region]->apiKey
        ]];
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->request('GET', $this->giftpay[$region]->apiBalance, $params);
            $data  = json_decode($response->getBody());
            $data->Region = $region;
            return $data;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $err = new stdClass();
            $err->message = $e->getMessage();
            $err->status = 'error';
            $err->statusCode = $e->getCode();
            return $err;
        }
    }

    private function getRawRates() {
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->request('GET', config('services.exchange.aud_rates'));
            $data  = json_decode($response->getBody())->rates;
            return $data;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $err = new stdClass();
            $err->message = $e->getMessage();
            $err->status = 'error';
            $err->statusCode = $e->getCode();
            return $err;
        }
    }
}
