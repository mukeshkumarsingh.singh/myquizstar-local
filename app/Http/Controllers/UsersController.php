<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAnswer;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

class UsersController extends Controller
{
    public function addAvatar(Request $request)
    {
        $user = $request->user();
        $avatar = $request->file('avatar');
        if (!isset($avatar)) {
            return response()->json([
                'status' => 'error',
                'message' => 'No avatar provided.'
            ], 200);
        }
        $extension = $avatar->getClientOriginalExtension();
        $filename = hash_hmac('ripemd160', $user->id, config('services.hash.key'));
        Storage::disk('public')->put($filename . '.' . $extension, File::get($avatar));
        $user->avatar_filename = $filename;
        $user->avatar_extension = $extension;
        $user->save();
        return response()->json([
            'status' => 'success',
            'data' => [
                'filename' => URL::to('storage/' . $filename . '.' . $extension)
            ]
        ], 200);
    }

    public function deleteAvatar(Request $request)
    {
        $user = $request->user();
        Storage::disk('public')->delete($user->avatar_filename . '.' . $user->avatar_extension);
        $user->avatar_filename = '';
        $user->avatar_extension = '';
        $user->save();
        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Avatar has been deleted.'
            ]
        ], 200);
    }

    public function addDeviceToken(Request $request) {
        $user = $request->user();
        $deviceToken = $request->token;
        $user->notification_token = $deviceToken;
        $user->save();
        if($user->stripe_id == null) {
            $user->stripe_id = '';
        }
        if($user->card_brand == null) {
            $user->card_brand = '';
        }
        if($user->card_last_four == null) {
            $user->card_last_four = '';
        }
        if($user->trial_ends_at == null) {
            $user->trial_ends_at = '';
        }
        $response = new \stdClass();
        $response->status = 'success';
        $response->data = $user;
        return response()->json($response, 200);
    }

    public function deleteDeviceToken(Request $request) {
        $user = $request->user();
        $user->notification_token = null;
        $user->save();
        if($user->stripe_id == null) {
            $user->stripe_id = '';
        }
        if($user->card_brand == null) {
            $user->card_brand = '';
        }
        if($user->card_last_four == null) {
            $user->card_last_four = '';
        }
        if($user->trial_ends_at == null) {
            $user->trial_ends_at = '';
        }
        $response = new \stdClass();
        $response->status = 'success';
        $response->data = $user;
        return response()->json($response, 200);
    }

    public function getUserDetails(Request $request)
    {
        $user = $request->user();
        unset($user->email_verified_at);
        if(isset($user->avatar_filename) && $user->avatar_filename !== '') {
            $user->avatar = URL::to('storage/' . $user->avatar_filename . '.' . $user->avatar_extension);
        }
        if(isset($user->avatar_filename)) {
            unset($user->avatar_filename);
        }
        if(isset($user->avatar_extension)) {
            unset($user->avatar_extension);
        }
        if($user->notifcation_token == null) {
            $user->notification_token = '';
        }
        if($user->stripe_id == null) {
            $user->stripe_id = '';
        }
        if($user->card_brand == null) {
            $user->card_brand = '';
        }
        if($user->card_last_four == null) {
            $user->card_last_four = '';
        }
        if($user->trial_ends_at == null) {
            $user->trial_ends_at = '';
        }

        $response = new \stdClass();
        $response->status = 'success';
        $response->data = $user;
        return response()->json($response, 200);
    }

    public function updateUserDetails(Request $request) {
        $user = $request->user();
        if(isset($request->firstname)) {
            $user->firstname = $request->firstname;
        }
        if(isset($request->lastname)) {
            $user->lastname = $request->lastname;
        }
        if(isset($request->region)) {
            $user->region = $request->region;
        }
        $user->save();
        unset($user->email_verified_at);
        if(isset($user->avatar_filename) && $user->avatar_filename !== '') {
            $user->avatar = URL::to($user->avatar_filename . '.' . $user->avatar_extension);
        }
        if(isset($user->avatar_filename)) {
            unset($user->avatar_filename);
        }
        if(isset($user->avatar_extension)) {
            unset($user->avatar_extension);
        }
        if($user->stripe_id == null) {
            $user->stripe_id = '';
        }
        if($user->card_brand == null) {
            $user->card_brand = '';
        }
        if($user->card_last_four == null) {
            $user->card_last_four = '';
        }
        if($user->trial_ends_at == null) {
            $user->trial_ends_at = '';
        }
        $response = new \stdClass();
        $response->status = 'success';
        $response->data = $user;
        return response()->json($response, 200);
    }
}
