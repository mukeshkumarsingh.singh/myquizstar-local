<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use Illuminate\Http\Request;
use App\UserProgress;
use App\UserAnswer;
use App\Balance;
use App\Transaction;
use App\User;
use App\UserQuestion;
use App\Challenge;
use App\KOGame;
use App\ScheduledKOGames;
use App\ScheduledKOGamesPlayers;
use App\ScheduledKOGamesQuestions;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class GameController extends Controller
{
    const EASY = 'Easy';
    const MEDIUM = 'Medium';
    const HARD = 'Hard';
    const DIFFICULT = 'Difficult';
    const VERY_DIFFICULT = 'Very Difficult';

    const WAITING = 'waiting';
    const IN_PROGRESS = 'in_progress';
    const COMPLETE = 'complete';

    const CREDIT = 'CREDIT';
    const DEBIT = 'DEBIT';

    const MYQUIZSTAR = 'My Quiz Star';
    const MYQUIZSTARZERO = 'My Quiz Star Zero';
    const MYQUIZSTARCHALLENGE = 'My Quiz Star Challenge';

    protected $progress = array();
    protected $winnings = array();
    protected $kels = array();
    protected $allowedChallengeBets = array();

    public function __construct()
    {
        $this->populateRuleset();
        $this->populateWinnings();
        $this->populateAllowedChallengeBets();
    }

    public function nextAvailableKOGames(Request $request)
    {
        $games = ScheduledKOGames::select(['id', 'scheduled_for', 'completed', 'k_o_games_id', 'status'])->where('status', $this::WAITING)->orWhere('status', $this::IN_PROGRESS)->where('completed', false)->with('KOGames:id,pot,pot,min_average,max_average,in_progress,cost_to_play')->get();
        $results = [];
        $userAverage = $this->getUserAverage();
        
        foreach ($games as $game) {
            if ($userAverage < $game->KOGames->min_average || $userAverage > $game->KOGames->max_average) {
                continue;
            }
            $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $game->id)->where('state', '<>', $this::COMPLETE)->count();
            $tmpGame = new \stdClass();
            $tmpGame->id = $game->id;
            if (Carbon::parse($game->scheduled_for) < Carbon::now()) {
                $tmpGame->next_game = Carbon::now()->diff(Carbon::parse($game->scheduled_for))->format('-%H:%I:%S');
            } else {
                $tmpGame->next_game = Carbon::now()->diff(Carbon::parse($game->scheduled_for))->format('%H:%I:%S');
            }
            // $tmpGame->status = $game->status;
            // $tmpGame->name = $game->KOGames->spreadsheet_identifier;
            $tmpGame->bet = $game->KOGames->pot;
            // $tmpGame->min_average = $game->KOGames->min_average / 100;
            // $tmpGame->max_average = $game->KOGames->max_average / 100;
            $tmpGame->players = $registeredPlayers;
            array_push($results, $tmpGame);
        }
        return response()->json([
            'status' => 'success',
            'data' => $results
        ], 200);
    }

    public function nextAvailableRegisteredKOGames(Request $request)
    {
        $games = ScheduledKOGamesPlayers::select(['id', 'scheduled_k_o_games_id', 'state'])->where('state', $this::WAITING)->where('user_id', $request->user()->id)->with('ScheduledKOGames.KOGames')->get();
        $results = [];
        
        foreach ($games as $game) {
            $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $game->ScheduledKOGames->id)->where('state', '<>', $this::COMPLETE)->count();
            $tmpGame = new \stdClass();
            $tmpGame->id = $game->ScheduledKOGames->id;
            if (Carbon::parse($game->ScheduledKOGames->scheduled_for) < Carbon::now()) {
                $tmpGame->next_game = Carbon::now()->diff(Carbon::parse($game->ScheduledKOGames->scheduled_for))->format('-%H:%I:%S');
            } else {
                $tmpGame->next_game = Carbon::now()->diff(Carbon::parse($game->ScheduledKOGames->scheduled_for))->format('%H:%I:%S');
            }
            // $tmpGame->status = $game->ScheduledKOGames->status;
            // $tmpGame->name = $game->ScheduledKOGames->KOGames->spreadsheet_identifier;
            $tmpGame->bet = $game->ScheduledKOGames->KOGames->pot;
            // $tmpGame->min_average = $game->ScheduledKOGames->KOGames->min_average / 100;
            // $tmpGame->max_average = $game->ScheduledKOGames->KOGames->max_average / 100;
            $tmpGame->players = $registeredPlayers;
            array_push($results, $tmpGame);
        }
        return response()->json([
            'status' => 'success',
            'data' => $results
        ], 200);
    }

    public function activeKOGames(Request $request) {
        $games = ScheduledKOGames::select('id', 'status')->where('status', $this::IN_PROGRESS)->where('completed', false)->get();
        // var_dump($games);
        // die();

        $results = [];
        
        foreach ($games as $game) {
            $gameProgresses = UserProgress::select('user_id','game', 'round', 'status')->where('scheduled_k_o_games_id', $game->id)->with('user:id,firstname,lastname,avatar_filename,avatar_extension')->get();
            $maxGame = 0;
            $maxRound = 0;
            $playersLeft = 0;
            foreach($gameProgresses as $progress) {
                $progress->game = $progress->game+1;
                $progress->round = $progress->round+1;
                if($maxGame >= $progress->game && $maxRound > $progress->round) {
                    $maxGame = $progress->game;
                    $maxRound = $progress->round;
                }
                if($progress->status == $this::IN_PROGRESS) {
                    $playersLeft = $playersLeft+1;
                }
                $progress->user->avatar = URL::to('storage/' . $progress->user->avatar_filename . '.' . $progress->user->avatar_extension);
                unset($progress->user->avatar_filename);
                unset($progress->user->avatar_extension);
            }
            $tmpGameProgress = new \stdClass();
            $tmpGameProgress->id = $game->id;
            $tmpGameProgress->status = $game->status;
            $tmpGameProgress->game = $maxGame;
            $tmpGameProgress->round = $maxRound;
            $tmpGameProgress->remaining_players = $playersLeft;
            $tmpGameProgress->players = $gameProgresses;
            // $scheduledGame = ScheduledKOGames::where('id', $scheduledGameProgress->scheduled_k_o_games_id)->with(['user', 'KOGames'])->get();
            array_push($results, $tmpGameProgress);
        }
        return response()->json([
            'status' => 'success',
            'data' => $results
        ], 200);
    }

    public function completedKOGames(Request $request) {
        $games = ScheduledKOGames::select('id', 'status', 'winner_id')->where('status', $this::COMPLETE)->where('completed', true)->with('user:id,firstname,lastname')->get();

        $results = [];
        
        foreach ($games as $game) {
            $gameProgresses = UserProgress::select('user_id','game', 'round', 'status')->where('scheduled_k_o_games_id', $game->id)->with('user:id,firstname,lastname,avatar_filename,avatar_extension')->get();
            $tmpGameProgress = new \stdClass();
            $tmpGameProgress->id = $game->id;
            $tmpGameProgress->status = $game->status;
            $tmpGameProgress->winner = $game->user;
            foreach($gameProgresses as $progress) {
                $progress->game = $progress->game+1;
                $progress->round = $progress->round+1;
                $progress->user->avatar = URL::to('storage/' . $progress->user->avatar_filename . '.' . $progress->user->avatar_extension);
                unset($progress->user->avatar_filename);
                unset($progress->user->avatar_extension);
            }
            $tmpGameProgress->players = $gameProgresses;

            // $scheduledGame = ScheduledKOGames::where('id', $scheduledGameProgress->scheduled_k_o_games_id)->with(['user', 'KOGames'])->get();
            array_push($results, $tmpGameProgress);
        }
        return response()->json([
            'status' => 'success',
            'data' => $results
        ], 200);
    }

    public function registerForKOGame(Request $request)
    {
        $scheduledGame = ScheduledKOGames::where('id', $request->id)->where('completed', false)->where('status', $this::WAITING)->first();
        if (!$scheduledGame) {
            return response()->json([
                'status' => 'error',
                'message' => 'No such game'
            ], 404);
        }
        $previousRegistation = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('user_id', Auth::user()->id)->where('state', '<>', $this::COMPLETE)->count();
        if ($previousRegistation > 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Already registered for this game.'
            ], 200);
        }
        if (Carbon::parse($scheduledGame->scheduled_for) < Carbon::now()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Game expired.'
            ], 200);
        }
        $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('state', '<>', $this::COMPLETE)->count();
        if ($registeredPlayers >= 250) {
            return response()->json([
                'status' => 'error',
                'message' => 'Game is at capacity.'
            ], 200);
        }

        //check if we meet the average
        $minAverage = $scheduledGame->KOGames->min_average / 100;
        $maxAverage = $scheduledGame->KOGames->max_average / 100;
        $userAverage = $this->getUserAverage();
        if ($userAverage < $minAverage) {
            return response()->json([
                'status' => 'error',
                'message' => 'User does not meet minimum average for this game.'
            ], 200);
        }
        if ($userAverage > $maxAverage) {
            return response()->json([
                'status' => 'error',
                'message' => 'User exceeds maximum average for this game.'
            ], 200);
        }
        $balance = $this->balance();
        if (($scheduledGame->KOGames->pot + 25) < $balance->balance) {
            // register user to this game
            $registeredGame = new ScheduledKOGamesPlayers();
            $registeredGame->user_id = Auth::user()->id;
            $registeredGame->scheduled_k_o_games_id = $scheduledGame->id;
            $registeredGame->state = $this::WAITING;
            $registeredGame->user_average = $userAverage;
            $registeredGame->save();

            $balance->balance = $balance->balance - 25;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = 25;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::DEBIT;
            $transaction->category = 'KO';
            $transaction->description = 'New KO Game started.';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();

            $balance->balance = $balance->balance - $scheduledGame->KOGames->pot;
            $balance->save();

            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = $scheduledGame->KOGames->pot;
            $transaction->balance = $balance->balance;
            $transaction->kels = 0;
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::DEBIT;
            $transaction->category = 'KO';
            $transaction->description = 'KO Game ' . $scheduledGame->KOGames->id . ' POT holding amount.';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();

            $registeredPlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('state', '<>', $this::COMPLETE)->get();
            $responseObj = new \StdClass();
            $responseObj->id = $scheduledGame->id;
            $responseObj->next_game = Carbon::now()->diff(Carbon::parse($scheduledGame->scheduled_for))->format('%H:%I:%S');
            $responseObj->name = $scheduledGame->KOGames->spreadsheet_identifier;
            $responseObj->bet = $scheduledGame->KOGames->pot;
            $responseObj->min_average = $scheduledGame->KOGames->min_average / 100;
            $responseObj->max_average = $scheduledGame->KOGames->max_average / 100;
            $responseObj->players = $registeredPlayers;

            return response()->json([
                'status' => 'success',
                'data' => $responseObj
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Insufficient balance.'
            ], 200);
        }
    }

    public function unregisterForKOGame(Request $request)
    {
        $scheduledGame = ScheduledKOGames::where('id', $request->id)->where('completed', false)->where('status', $this::WAITING)->first();
        if (!$scheduledGame) {
            return response()->json([
                'status' => 'error',
                'message' => 'No such game'
            ], 404);
        }
        $registeredGame = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledGame->id)->where('user_id', Auth::user()->id)->where('state', '<>', $this::COMPLETE)->first();
        if (!$registeredGame) {
            return response()->json([
                'status' => 'error',
                'message' => 'Not registered for this game.'
            ], 200);
        }
        if (Carbon::parse($scheduledGame->scheduled_for) < Carbon::now()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Game expired.'
            ], 200);
        }

        $registeredGame->state = $this::COMPLETE;
        $registeredGame->save();

        $balance = $this->balance();
        $balance->balance = $balance->balance + 25;
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        $transaction->amount = 25;
        $transaction->balance = $balance->balance;
        $transaction->kels = 0;
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::CREDIT;
        $transaction->category = 'KO';
        $transaction->description = 'KO Game unregistered.';
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();

        $balance->balance = $balance->balance - $scheduledGame->KOGames->pot;
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        $transaction->amount = $scheduledGame->KOGames->pot;
        $transaction->balance = $balance->balance;
        $transaction->kels = 0;
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::CREDIT;
        $transaction->category = 'KO';
        $transaction->description = 'KO Game ' . $scheduledGame->KOGames->id . ' POT holding returned.';
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();

        return response()->json([
            'status' => 'success',
            'data' => 'Unregistered from game ' . $scheduledGame->KOGames->id
        ], 200);
    }

    public function getKOResults()
    {
        $scheduledGamesProgresses = UserProgress::where('user_id', Auth::user()->id)->where('scheduled_k_o_games_id', '<>', null)->get();
        $result = [];
        foreach ($scheduledGamesProgresses as $scheduledGameProgress) {
            $scheduledGame = ScheduledKOGames::where('id', $scheduledGameProgress->scheduled_k_o_games_id)->with(['user', 'KOGames'])->get();
            array_push($result, $scheduledGame);
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    public function challengesSent(Request $request)
    {
        $rankings = $this->rankUsersWithAdditionalDetail();
        $challenges = challenge::select(['id', 'bet_amount', 'user_2_id', 'winner_id', 'win_amount', 'challenge_accepted'])->where('user_1_id', Auth::user()->id)->get();
        foreach ($challenges as $challenge) {
            $challenge->versus = $challenge->user_2_id;
            if ($challenge->winner_id !== null) {
                if ($challenge->winner_id == $challenge->user_2_id) {
                    $challenge->state = 'lost';
                    $challenge->won = 0;
                } else if ($challenge->winner_id == 0) {
                    $challenge->state = 'returned';
                    $challenge->won = 0;
                } else {
                    $challenge->state = 'won';
                    $challenge->won = $challenge->win_amount;
                }
            } else {
                if ($challenge->challenge_accepted) {
                    $challenge->state = 'accepted';
                } else {
                    $challenge->state = 'waiting';
                }
            }

            $fromProgress = UserProgress::select(['game', 'round', 'status'])->where('user_id', Auth::user()->id)->where('challenge_id', $challenge->id)->first();
            if ($fromProgress) {
                $fromProgress->game = $fromProgress->game + 1;
                $fromProgress->round = $fromProgress->round + 1;
            } else {
                $fromProgress = new \stdClass();
            }
            $rankings[Auth::user()->id]['progress'] = $fromProgress;
            $toProgress = UserProgress::select(['game', 'round', 'status'])->where('user_id', $challenge->user_2_id)->where('challenge_id', $challenge->id)->first();
            if ($toProgress) {
                $toProgress->game = $toProgress->game + 1;
                $toProgress->round = $toProgress->round + 1;
            } else {
                $toProgress = new \stdClass();
            }
            $rankings[$challenge->user_2_id]['progress'] = $toProgress;
            $challenge->from = $rankings[Auth::user()->id];
            $challenge->to = $rankings[$challenge->user_2_id];
            unset($challenge->user_2_id);
            unset($challenge->winner_id);
            unset($challenge->win_amount);
            unset($challenge->challenge_accepted);
        }
        return response()->json([
            'status' => 'success',
            'data' => $challenges
        ], 200);
    }

    public function challengesReceived(Request $request)
    {
        $rankings = $this->rankUsersWithAdditionalDetail();
        $challenges = challenge::select(['id', 'bet_amount', 'user_1_id', 'winner_id', 'win_amount', 'challenge_accepted'])->where('user_2_id', Auth::user()->id)->where(function ($query) {
            $query->where('winner_id', '!=', 0)->orWhereNull('winner_id');
        })->get();

        foreach ($challenges as $challenge) {
            $challenge->versus = $challenge->user_1_id;
            if ($challenge->winner_id !== null) {
                if ($challenge->winner_id == $challenge->user_1_id) {
                    $challenge->state = 'lost';
                    $challenge->won = 0;
                } else if ($challenge->winner_id == 0) {
                    $challenge->state = 'returned';
                    $challenge->won = 0;
                } else {
                    $challenge->state = 'won';
                    $challenge->won = $challenge->win_amount;
                }
            } else {
                if ($challenge->challenge_accepted) {
                    $challenge->state = 'accepted';
                } else {
                    $challenge->state = 'waiting';
                }
            }
            $fromProgress = UserProgress::select(['game', 'round', 'status'])->where('user_id', $challenge->user_1_id)->where('challenge_id', $challenge->id)->first();
            if ($fromProgress) {
                $fromProgress->game = $fromProgress->game + 1;
                $fromProgress->round = $fromProgress->round + 1;
            } else {
                $fromProgress = new \stdClass();
            }
            $rankings[$challenge->user_1_id]['progress'] = $fromProgress;
            $toProgress = UserProgress::select(['game', 'round', 'status'])->where('user_id', Auth::user()->id)->where('challenge_id', $challenge->id)->first();
            if ($toProgress) {
                $toProgress->game = $toProgress->game + 1;
                $toProgress->round = $toProgress->round + 1;
            } else {
                $toProgress = new \stdClass();
            }
            $rankings[Auth::user()->id]['progress'] = $toProgress;
            $challenge->from = $rankings[$challenge->user_1_id];
            $challenge->to = $rankings[Auth::user()->id];
            unset($challenge->user_1_id);
            unset($challenge->winner_id);
            unset($challenge->win_amount);
            unset($challenge->challenge_accepted);
        }
        return response()->json([
            'status' => 'success',
            'data' => $challenges
        ], 200);
    }

    public function returnChallenge(Request $request)
    {
        if ($request->challenge) {
            $challenge = Challenge::where('id', $request->challenge)->where('user_2_id', Auth::user()->id)->first();
            if ($challenge) {
                // var_dump($challenge->challenge_accepted);
                if ($challenge->challenge_accepted == true) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Unable to return, challenge already accepted.'
                    ], 200);
                } else {
                    $challenge = Challenge::find($challenge->id);
                    $challenge->winner_id = 0;
                    $challenge->save();

                    $balance = Balance::where('user_id', $challenge->user_1_id)->first();
                    $balance->balance = $balance->balance + 20;
                    $balance->save();

                    $transaction = new Transaction();
                    $transaction->user_id = Auth::user()->id;
                    $transaction->amount = 20;
                    $transaction->balance = $balance->balance;
                    $transaction->kels = 0;
                    $transaction->kels_balance = $balance->kels;
                    $transaction->type = self::CREDIT;
                    $transaction->category = 'Challenge';
                    $transaction->description = 'Challenge cost to play returned.';
                    $transaction->reference = $this->getUniqueTransactionReference();
                    $transaction->save();

                    $balance->balance = $balance->balance + $challenge->bet_amount;
                    $balance->save();

                    $transaction = new Transaction();
                    $transaction->user_id = Auth::user()->id;
                    $transaction->amount = $challenge->bet_amount;
                    $transaction->balance = $balance->balance;
                    $transaction->kels = 0;
                    $transaction->kels_balance = $balance->kels;
                    $transaction->type = self::CREDIT;
                    $transaction->category = 'Challenge';
                    $transaction->description = 'Challenge ID: ' . $challenge->id . ' returned. Bet amount ' . $challenge->bet_amount . '.';
                    $transaction->reference = $this->getUniqueTransactionReference();
                    $transaction->save();
                    return $this->challengesReceived($request);
                }
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'No matching challenge found.'
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid challenge ID'
            ], 200);
        }
    }

    public function challenge(Request $request)
    {
        //bail if we already have a game in progress
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if ($userProgress) {
            return response()->json([
                'status' => 'error',
                'message' => 'Game already in progress. Finish before starting a challenge.'
            ], 200);
        }
        //get the versus user and create a challenge
        if (strcasecmp($request->action, 'start') == 0) {
            if ($request->versus) {
                if ((int) $request->versus === Auth::user()->id) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Invalid challenge versus. Can\'t challenge yourself.'
                    ], 200);
                }
                $challengedUser = User::where('id', $request->versus)->first();
                if ($challengedUser) {
                    if ($request->bet) {
                        if ($this->isAllowedChallengeBet($request->bet)) {
                            //ensure we have sufficient balance for bet and play and deduct bet
                            $balance = $this->balance();
                            if (($request->bet + 20) < $balance->balance) {
                                $challenge = new Challenge();
                                $challenge->bet_amount = $request->bet;
                                $challenge->user_1_id = Auth::user()->id;
                                $challenge->user_2_id = $challengedUser->id;
                                $challenge->challenge_accepted = false;
                                $challenge->save();

                                $balance->balance = $balance->balance - 20;
                                $balance->save();

                                $transaction = new Transaction();
                                $transaction->user_id = Auth::user()->id;
                                $transaction->amount = 20;
                                $transaction->balance = $balance->balance;
                                $transaction->kels = 0;
                                $transaction->kels_balance = $balance->kels;
                                $transaction->type = self::DEBIT;
                                $transaction->category = 'Challenge';
                                $transaction->description = 'New Challenge Game started.';
                                $transaction->reference = $this->getUniqueTransactionReference();
                                $transaction->save();

                                $balance->balance = $balance->balance - $request->bet;
                                $balance->save();

                                $transaction = new Transaction();
                                $transaction->user_id = Auth::user()->id;
                                $transaction->amount = $request->bet;
                                $transaction->balance = $balance->balance;
                                $transaction->kels = 0;
                                $transaction->kels_balance = $balance->kels;
                                $transaction->type = self::DEBIT;
                                $transaction->category = 'Challenge';
                                $transaction->description = 'Request new challenge bet holding amount.';
                                $transaction->reference = $this->getUniqueTransactionReference();
                                $transaction->save();

                                $responseObj = new \StdClass();
                                $responseObj->challenge_id = $challenge->id;
                                $responseObj->bet_amount = (int) $challenge->bet_amount;
                                $responseObj->versus = $challenge->user_2_id;

                                return response()->json([
                                    'status' => 'success',
                                    'data' => $responseObj
                                ], 200);
                            } else {
                                return response()->json([
                                    'status' => 'error',
                                    'message' => 'Insufficient balance.'
                                ], 200);
                            }
                        } else {
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Invalid challenge bet. Valid bet amounts (in cents) are: 20, 50, 75, 100, 150, 200, 250, 500, 1000, 1500, 2000, 2500, 5000'
                            ], 200);
                        }
                    } else {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Invalid challenge bet. Provide a bet amount.'
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Invalid challenge versus. No such user.'
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid challenge versus. Provide an user ID to challenge.'
                ], 200);
            }
        } elseif (strcasecmp($request->action, 'accept') == 0) {
            if ($request->challenge) {
                $challenge = Challenge::where('id', $request->challenge)->where('user_2_id', Auth::user()->id)->first();
                if ($challenge) {
                    // var_dump($challenge->challenge_accepted);
                    if ($challenge->challenge_accepted == true) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Challenge already accepted.'
                        ], 200);
                    } else {
                        $balance = $this->balance();
                        if (($request->bet + 20) < $balance->balance) {
                            $challenge->challenge_accepted = true;
                            $challenge->save();

                            $balance->balance = $balance->balance - 20;
                            $balance->save();

                            $transaction = new Transaction();
                            $transaction->user_id = Auth::user()->id;
                            $transaction->amount = 20;
                            $transaction->balance = $balance->balance;
                            $transaction->kels = 0;
                            $transaction->kels_balance = $balance->kels;
                            $transaction->type = self::DEBIT;
                            $transaction->category = 'Challenge';
                            $transaction->description = 'New Challenge Game started.';
                            $transaction->reference = $this->getUniqueTransactionReference();
                            $transaction->save();

                            $balance->balance = $balance->balance - $challenge->bet_amount;
                            $balance->save();

                            $transaction = new Transaction();
                            $transaction->user_id = Auth::user()->id;
                            $transaction->amount = $challenge->bet_amount;
                            $transaction->balance = $balance->balance;
                            $transaction->kels = 0;
                            $transaction->kels_balance = $balance->kels;
                            $transaction->type = self::DEBIT;
                            $transaction->category = 'Challenge';
                            $transaction->description = 'Accept challenge bet holding amount.';
                            $transaction->reference = $this->getUniqueTransactionReference();
                            $transaction->save();

                            $responseObj = new \StdClass();
                            $responseObj->challenge_id = $challenge->id;
                            $responseObj->bet_amount = $challenge->bet_amount;
                            $responseObj->versus = $challenge->user_1_id;

                            return response()->json([
                                'status' => 'success',
                                'data' => $responseObj
                            ], 200);
                        } else {
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Insufficient balance.'
                            ], 200);
                        }
                    }
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'No matching challenge found.'
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unable to accept invalid challenge ID'
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid challenge action. Use start or accept.'
            ], 200);
        }
    }

    public function startGame(Request $request)
    {
        // check the if the user has any current games running
        // if not start a new one, otherwise point to current active game
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        $balance = $this->balance();
        $gameType = self::MYQUIZSTAR;
        //get the game type
        if ($request->game) {
            if (strcasecmp($request->game, 'zero') == 0) {
                $gameType = self::MYQUIZSTARZERO;
            } elseif (strcasecmp($request->game, 'challenge') == 0) {
                $gameType = self::MYQUIZSTARCHALLENGE;
            }
        }

        if (!$userProgress) {
            // only allow play on zero balance if QS zero
            if ($gameType != self::MYQUIZSTARZERO) {
                if ($gameType == self::MYQUIZSTARCHALLENGE) {
                    // check if we have sufficient balance
                    if ($balance->kels < 2) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Insufficient Kels balance.'
                        ], 200);
                    }
                    // get the ID of the challenge and start that
                    if ($request->id) {
                        $challenge = challenge::where('id', $request->id)->where('winner_id', null)->first();
                        if (!$challenge) {
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Invalid challenge ID or Challenge already completed.'
                            ], 200);
                        }
                        if ($challenge->user_2_id == Auth::user()->id && $challenge->challenge_accepted != true) {
                            // check if they have accepted challenge first
                            // var_dump($challenge->challenge_accepted);
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Challenge not accepted, accept challenge first.'
                            ], 200);
                        }
                        //check if we've run this before
                        $prevProgress = UserProgress::where('user_id', Auth::user()->id)->where('challenge_id', $request->id)->first();
                        if ($prevProgress) {
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Challenge allready accepted, please choose another.'
                            ], 200);
                        }
                    } else {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'No challenge ID given.'
                        ], 200);
                    }
                } else {
                    if ($balance->kels < 1) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Insufficient Kels balance.'
                        ], 200);
                    }
                    if ($balance->balance < 15) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Insufficient balance.'
                        ], 200);
                    }
                }
            }

            //start a new game
            $newGame = new UserProgress();
            $newGame->user_id = Auth::user()->id;
            $newGame->game = 0;
            $newGame->round = 0;
            $newGame->status = self::IN_PROGRESS;
            $newGame->question_id = 0;
            $newGame->type = $gameType;
            if (isset($challenge)) {
                $newGame->challenge_id = $challenge->id;
            }
            $newGame->save();

            $question = $this->getQuestion();

            if (!$question) {
                // no game active
                return response()->json([
                    'status' => 'error',
                    'message' => 'No more games available at this time. Please contact support.'
                ], 200);
            }
            $newGame->question_id = $question->id;

            try {
                $newGame->save();

                $transactionAmount = 0;
                $kelsTransactionAmount = 0;
                if ($gameType != self::MYQUIZSTARZERO) {
                    if ($gameType == self::MYQUIZSTARCHALLENGE) {
                        $balance->kels = $balance->kels - 2;
                        $kelsTransactionAmount = 2;
                    } else {
                        if ($balance->balance > 15) {
                            $balance->balance = $balance->balance - 15;
                            $transactionAmount = 15;
                        }
                        $balance->kels = $balance->kels - 1;
                        $kelsTransactionAmount = 1;
                    }
                } else {
                    $balance->kels = $balance->kels - 1;
                    $kelsTransactionAmount = 1;
                }
                $balance->save();

                $transaction = new Transaction();
                $transaction->user_id = Auth::user()->id;
                $transaction->amount = $transactionAmount;
                $transaction->balance = $balance->balance;
                $transaction->kels = $kelsTransactionAmount;
                $transaction->kels_balance = $balance->kels;
                $transaction->type = self::DEBIT;
                $transaction->category = 'Play Game';
                $transaction->description = 'New game started';
                $transaction->reference = $this->getUniqueTransactionReference();
                $transaction->save();
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unfortunately, we're unable to debit your account at this time. Please contact support."
                ], 500);
            }


            $questionCategories = $this->getCategoriesById($question->category_id);

            return response()->json([
                'status' => 'success',
                'data' => [
                    'game' => $newGame->game + 1,
                    'round' => $newGame->round + 1,
                    'status' => $newGame->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($newGame->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($newGame->round + 1) / 10) * 100,
                    'question_id' => $question->id,
                    'question' => $question->question,
                    'a' => $question->option_a,
                    'b' => $question->option_b,
                    'c' => $question->option_c,
                    'd' => $question->option_d,
                    'difficulty' => $question->difficulty,
                    'primary_category' => $questionCategories->primary_category,
                    'secondary_category' => $questionCategories->secondary_category
                ]
            ], 200);
        } else {
            if ($userProgress->question_id == 0) {
                // either start a new game
                $question = $this->getQuestion();
                if (!$question) {
                    // no game active
                    return response()->json([
                        'status' => 'error',
                        'message' => 'No more games available at this time. Please contact support.'
                    ], 200);
                }
            } else {
                // or tell them where they are
                $question = $this->getQuestionById($userProgress->question_id);
            }
            $questionCategories = $this->getCategoriesById($question->category_id);
            return response()->json([
                'status' => 'success',
                'data' => [
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100,
                    'question_id' => $question->id,
                    'question' => $question->question,
                    'a' => $question->option_a,
                    'b' => $question->option_b,
                    'c' => $question->option_c,
                    'd' => $question->option_d,
                    'difficulty' => $question->difficulty,
                    'primary_category' => $questionCategories->primary_category,
                    'secondary_category' => $questionCategories->secondary_category
                ]
            ], 200);
        }
    }

    public function allowedBets(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'allowed_bets' => array_keys($this->allowedChallengeBets)
            ]
        ], 200);
    }

    public function getProgress()
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        } else {
            //tell them where they are
            return response()->json([
                'status' => 'success',
                'data' => [
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100,
                ]
            ], 200);
        }
    }

    public function getCurrentKOQuestion()
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->where('scheduled_k_o_games_id', '<>', null)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active.'
            ], 200);
        }
        $question = $this->getQuestionById($userProgress->question_id);
        if (!$question) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, did you register?'
            ], 200);
        } else {
            $questionCategories = $this->getCategoriesById($question->category_id);
            return response()->json([
                'status' => 'success',
                'data' => [
                    'question_id' => $question->id,
                    'question' => $question->question,
                    'a' => $question->option_a,
                    'b' => $question->option_b,
                    'c' => $question->option_c,
                    'd' => $question->option_d,
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'difficulty' => $question->difficulty,
                    'primary_category' => $questionCategories->primary_category,
                    'secondary_category' => $questionCategories->secondary_category
                ]
            ], 200);
        }
    }

    public function getCurrentQuestion()
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        }
        if ($userProgress->status == self::COMPLETE) {
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        }
        if ($userProgress->round == 0 && $userProgress->question_id == 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'No game in progress, start one using the start endpoint.'
            ], 200);
        }
        $question = $this->getQuestion();
        if (!$question) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        } else {
            $questionCategories = $this->getCategoriesById($question->category_id);
            return response()->json([
                'status' => 'success',
                'data' => [
                    'question_id' => $question->id,
                    'question' => $question->question,
                    'a' => $question->option_a,
                    'b' => $question->option_b,
                    'c' => $question->option_c,
                    'd' => $question->option_d,
                    'difficulty' => $question->difficulty,
                    'primary_category' => $questionCategories->primary_category,
                    'secondary_category' => $questionCategories->secondary_category
                ]
            ], 200);
        }
    }

    public function bank(Request $request)
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active.'
            ], 200);
        }

        /* if ($userProgress->round != 9) {
            // var_dump($userProgress->round);
            return response()->json([
                'status' => 'error',
                'message' => 'Not enough progress to bank winnings.'
            ], 200);
        } */

        $balance = $this->balance();

        if (isset($userProgress->type) && $userProgress->type == self::MYQUIZSTAR) {
            $winAmount = $this->winnings[$userProgress->game];
            $balance->balance = $balance->balance + $winAmount;
        }
		
		#ADD BANK FOR ZERO GAME
		if (isset($userProgress->type) && $userProgress->type == self::MYQUIZSTARZERO) {
            $winAmount = $this->winnings[$userProgress->game];
            $balance->balance = $balance->balance + $winAmount;
        }

        $kelsAmount = $this->kels[$userProgress->game];
        $balance->kels = $balance->kels + $kelsAmount;
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        $transaction->amount = $winAmount;
        $transaction->balance = $balance->balance;
        $transaction->kels = $kelsAmount;
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::CREDIT;
        $transaction->category = 'Game Win';
        $transaction->description = 'Round ' . ($userProgress->game + 1) . ' winnings banked';
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();

        $userProgress->status = self::COMPLETE;
        $userProgress->save();

        return response()->json([
            'status' => 'success',
            'data' => [
                'balance' => $balance->balance,
                'kels' => $balance->kels
            ]
        ], 200);
    }

    public function endGame(Request $request)
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, unable to end game.'
            ], 200);
        }

        $userQuestion = UserQuestion::where('user_id', Auth::user()->id)->where('user_progresses_id', $userProgress->id)->first();
        $question = $this->getQuestionById($userProgress->question_id);

        $userAnswer = new UserAnswer();
        $userAnswer->question_id = $userQuestion->question_id;
        $userAnswer->user_id = Auth::user()->id;
        $userAnswer->given_answer = 'endgame';
        $userAnswer->correct_answer = '';
        $userAnswer->answered_correctly = false;
        $userAnswer->user_question_id = $userQuestion->id;
        $userAnswer->save();

        $userProgress->status = self::COMPLETE;
        $userProgress->save();

        if (isset($userProgress->challenge_id)) {
            $this->checkForChallengeWin($userProgress->challenge_id);
        } else if (isset($userProgress->scheduled_k_o_games_id)) {
            $this->markKOIncorrect($userProgress->scheduled_k_o_games_id);
        }
        
        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Game ended.'
            ]
        ], 200);
    }

    public function answerQuestion(Request $request)
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        }
        if ($userProgress->question_id == 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Game is already in progress, but no active question found, please call the question endpoint first.'
            ], 200);
        }
        if (isset($userProgress->scheduled_k_o_games_id)) {
            $question = $this->getQuestionById($userProgress->question_id);
        } else {
            $question = $this->getQuestion();

            $existingAnswer = UserAnswer::where('question_id', $question->id)->where('user_id', Auth::user()->id)->first();
            if ($existingAnswer) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid request. This question has already been answered and cannot be answered again.'
                ], 400);
            }
        }

        $answer = strtolower($request->answer);
        if (!$answer) {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid request. You must provide an answer.'
            ], 400);
        }
        $userQuestion = UserQuestion::where('user_id', Auth::user()->id)->where('question_id', $question->id)->first();

        $userAnswer = new UserAnswer();
        $userAnswer->question_id = $question->id;
        $userAnswer->user_id = Auth::user()->id;
        $userAnswer->given_answer = $answer;
        $userAnswer->user_question_id = $userQuestion->id;
        $userAnswer->correct_answer = $question->answer;

        if (strtolower($question->answer) == $answer) {
            //correct
            $userAnswer->answered_correctly = true;
            $userAnswer->save();
            // move the game forward or win
            if ($userProgress->round < 9) {
                $userProgress->round = $userProgress->round + 1;
            } else {
                // round complete call bank or new game
                if ($userProgress->game == 9) {
                    if (!isset($userProgress->challenge_id)) {
                        $userProgress->status = self::COMPLETE;
                        $winAmount = $this->winnings[$userProgress->game];
                        $kelsAmount = $this->kels[$userProgress->game];
                        $balance = $this->balance();
                        $balance->balance = $balance->balance + $winAmount;
                        $balance->kels = $balance->kels + $kelsAmount;
                        $balance->save();

                        $transaction = new Transaction();
                        $transaction->user_id = Auth::user()->id;
                        $transaction->amount = $winAmount;
                        $transaction->balance = $balance->balance;
                        $transaction->kels = $kelsAmount;
                        $transaction->kels_balance = $balance->kels;
                        $transaction->type = self::CREDIT;
                        $transaction->category = 'Game Win';
                        $transaction->description = 'Round ' . ($userProgress->game + 1) . ' winnings banked';
                        $transaction->reference = $this->getUniqueTransactionReference();
                        $transaction->save();
                    }
                } else {
                    $userProgress->game = $userProgress->game + 1;
                    $userProgress->round = 0;
                }
            }
            $userProgress->question_id = 0;
            $userProgress->save();
            if (isset($userProgress->challenge_id)) {
                $this->checkForChallengeWin($userProgress->challenge_id);
            } else if (isset($userProgress->scheduled_k_o_games_id)) {
                $this->markKOCorrect($userProgress->scheduled_k_o_games_id, $userAnswer->id);
            }
            return response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $question->id,
                    'correct' => true,
                    'correct_answer' => $question->answer,
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100
                ]
            ], 200);
        } else {
            //incorrect
            $userAnswer->answered_correctly = false;
            $userAnswer->save();
            // $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
            $userProgress->status = self::COMPLETE;
            $userProgress->save();
            if (isset($userProgress->challenge_id)) {
                $this->checkForChallengeWin($userProgress->challenge_id);
            } else if (isset($userProgress->scheduled_k_o_games_id)) {
                $this->markKOIncorrect($userProgress->scheduled_k_o_games_id);
            }
            return response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $question->id,
                    'correct' => false,
                    'correct_answer' => $question->answer,
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100
                ]
            ], 200);
        }
    }

    private function updateChallengeWin($winnerId, $challengeId)
    {
        $challengeProgresses = UserProgress::where('challenge_id', $challengeId)->get();

        foreach($challengeProgresses as $progress) {
            $progress->status = self::COMPLETE;
            $progress->save();
        }

        $challenge = Challenge::find($challengeId);
        $challenge->winner_id = $winnerId;
        $challenge->win_amount = ($challenge->bet_amount * 0.9);
        $challenge->save();

        $balance = Balance::where('user_id', $winnerId)->first();
        $balance->balance = $balance->balance + $challenge->bet_amount + $challenge->win_amount;
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = $winnerId;
        $transaction->amount = $challenge->bet_amount + $challenge->win_amount;
        $transaction->balance = $balance->balance;
        $transaction->kels = 0;
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::CREDIT;
        $transaction->category = 'Challenge Win';
        $transaction->description = 'Challenge ID: ' . $challenge->id . ' won. From challenger:  ' . $challenge->win_amount . ' (bet amount less 10% fee) plus your original bet of ' . $challenge->bet_amount;
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();
    }


    private function checkForChallengeWin($challengeId)
    {
        $userProgresses = UserProgress::where('challenge_id', $challengeId)->get();
        foreach ($userProgresses as $userProgress) {
            if ($userProgress->user_id == Auth::user()->id) {
                $myProgress = $userProgress;
            } else {
                $theirProgress = $userProgress;
            }
        }
        $myPosition = ((int) $userProgress->game * 10) + (int) $userProgress->round;
        if (isset($theirProgress)) {
            $theirPosition = ((int) $theirProgress->game * 10) + (int) $theirProgress->round;
        } else {
            return false;
        }

        if($myPosition > $theirPosition) {
            if ($theirProgress->status == self::COMPLETE) {
                $this->updateChallengeWin($myProgress->user_id, $myProgress->challenge_id);
                return;
            }
        } else if ($myPosition < $theirPosition) {
            if ($myProgress->status == self::COMPLETE) {
                $this->updateChallengeWin($theirProgress->user_id, $theirProgress->challenge_id);
                return;
            }
        } else {
            if ($myProgress->status == self::COMPLETE && $theirProgress->status == self::COMPLETE) {
                $this->updateChallengeWin($this->challengeTimeWinner($myProgress, $theirProgress), $myProgress->challenge_id);
                return;
            }
        }

        // if ($theirProgress->status == self::COMPLETE) {
        //     // we're done so we could be the winner
        //     if ($myPosition > $theirPosition) {
        //         $this->updateChallengeWin($myProgress->user_id, $myProgress->challenge_id);
        //         return;
        //     } else if ($myPosition == $theirPosition) {
        //         $this->updateChallengeWin($this->challengeTimeWinner($myProgress, $theirProgress), $myProgress->challenge_id);
        //         return;
        //     } 
        //     else {
        //         if ($myProgress->status == self::COMPLETE) {
        //             $this->updateChallengeWin($theirProgress->user_id, $theirProgress->challenge_id);
        //             return;
        //         }
        //         return;
        //     }
        // } 

    }

    private function markKOCorrect($scheduledKOGameId, $userAnswerId)
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->where('scheduled_k_o_games_id', $scheduledKOGameId)->first();
        if (!$userProgress) {
            return false;
        }

        //check if we have won
        $winner = $this->checkForKOWin($scheduledKOGameId);
        if ($winner != null) {
            // we have a winner
            $userAnswer = UserAnswer::where('id', $userAnswerId)->first();
            $userAnswer->given_answer = 'ko_winner';
            $userAnswer->save();
            $userProgress->status = self::COMPLETE;
            $userProgress->save();
            $this->markKOComplete($scheduledKOGameId, $winner);
        } else {
            // still going
            // correct so get next question
            $nextQuestion = ScheduledKOGamesQuestions::where('scheduled_k_o_games_id', $scheduledKOGameId)->where('game', $userProgress->game)->where('round', $userProgress->round)->first();
            $userProgress->question_id = $nextQuestion->questions_id;
            $userProgress->save();

            $userQuestion = new UserQuestion();
            $userQuestion->user_id = Auth::user()->id;
            $userQuestion->question_id = $nextQuestion->questions_id;
            $userQuestion->game = $userProgress->game;
            $userQuestion->round = $userProgress->round;
            $userQuestion->user_progresses_id = $userProgress->id;
            $userQuestion->save();
        }
    }

    private function markKOIncorrect($scheduledKOGameId)
    {
        $winner = $this->checkForKOWin($scheduledKOGameId);
        if ($winner != null) {
            //we have a winner
            $this->markKOComplete($scheduledKOGameId, $winner);
        }
    }

    private function markKOComplete($scheduledKOGameId, $winner)
    {
        $scheduledGame = ScheduledKOGames::find($scheduledKOGameId);
        $scheduledGame->status = $this::COMPLETE;
        $scheduledGame->completed = true;
        $scheduledGame->winner_id = $winner;
        $scheduledGame->save();

        $scheduledGamePlayers = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledKOGameId)->get();
        foreach ($scheduledGamePlayers as $player) {
            $player->state = $this::COMPLETE;
            $player->save();
        }
    }

    private function checkForKOWin($scheduledKOGameId)
    {
        //get all the users playing this KO and see if they have finished
        $players = ScheduledKOGamesPlayers::where('scheduled_k_o_games_id', $scheduledKOGameId)->get();
        $winnerId = null;
        $winnerProgressId = null;
        $position = 0;
        foreach ($players as $player) {
            $userProgress = UserProgress::where('user_id', $player->user_id)->where('scheduled_k_o_games_id', $scheduledKOGameId)->first();
            if ($userProgress && $userProgress->status != $this::COMPLETE) {
                //someone is still playing, no winner yet
                $winnerId = null;
                break;
            } else {
                $curPos = $userProgress->game + 1 * $userProgress->round + 1;
                if ($curPos > $position) {
                    $winnerId = $userProgress->user_id;
                    $winnerProgressId = $userProgress->id;
                    $position = $curPos;
                } else if ($curPos == $position) {
                    if ($winnerId == null) {
                        $winnerId = $userProgress->user_id;
                        $winnerProgressId = $userProgress->id;
                    } else {
                        $winnerId = $this->tiebreakKO($userProgress->id, $winnerProgressId);
                        if ($winnerId == $userProgress->user_id) {
                            $winnerProgressId = $userProgress->id;
                        }
                    }
                }
            }
        }
        return $winnerId;
    }

    private function tiebreakKO($currentProgressId, $previousProgressId)
    {
        $latestQuestionCurrent = UserQuestion::where('user_progresses_id', $currentProgressId)->latest('created_at')->first();
        $latestAnswerCurrent = UserAnswer::where('user_question_id', $latestQuestionCurrent->id)->latest('created_at')->first();
        $latestQuestionPrevious = UserQuestion::where('user_progresses_id', $previousProgressId)->latest('created_at')->first();
        $latestAnswerPrevious = UserAnswer::where('user_question_id', $latestQuestionPrevious->id)->latest('created_at')->first();
        if ($latestAnswerCurrent->created_at->gt($latestAnswerPrevious->created_at)) {
            return $latestAnswerPrevious->user_id;
        } else {
            return $latestAnswerCurrent->user_id;
        }
    }

    private function challengeTimeWinner($myProgress, $theirProgress)
    {
        $myQuestions = UserQuestion::where('user_progresses_id', $myProgress->id);
        $theirQuestions = UserQuestion::where('user_progresses_id', $theirProgress->id);
        $mySpeed = 0;
        $theirSpeed = 0;

        foreach ($myQuestions as $myQuestion) {
            $myAnswer = UserAnswer::where('user_question_id', $myQuestion->id);
            $mySpeed = $mySpeed + $myAnswer->created_at->diffInSeconds($myQuestion->created_at);
        }

        foreach ($theirQuestions as $theirQuestion) {
            $theirAnswer = UserAnswer::where('user_question_id', $myQuestion->id);
            $theirSpeed = $theirSpeed + $theirAnswer->created_at->diffInSeconds($theirQuestion->created_at);
        }

        if ($mySpeed < $theirSpeed) {
            return $myProgress->user_id;
        } else {
            return $theirProgress->user_id;
        }
    }

    private function getQuestion()
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->with('challenge')->first();

        if (!$userProgress) {
            return null;
        } else {
            // if we already have a question ID, return that
            if ($userProgress->question_id != 0) {
                return $this->getQuestionById($userProgress->question_id);
            } else {
                $askNewQuestion = false;
                if ($userProgress->challenge_id) {
                    // get the most advanced position from both users
                    $challengerId = null;
                    if ($userProgress->challenge->user_1_id == $userProgress->user_id) {
                        $challengerId = $userProgress->challenge->user_2_id;
                    } else {
                        $challengerId = $userProgress->challenge->user_1_id;
                    }
                    $challengerProgress = UserProgress::where('user_id', $challengerId)->where('challenge_id', $userProgress->challenge_id)->first();
                    if (!$challengerProgress) {
                        // if the challenger has made no progress, ask a new question
                        $askNewQuestion = true;
                    } else {
                        // if the challenger has made progress
                        // if we're ahead, askNewQuestion
                        // otherwise ask the same question during their progress
                        $myProgress = ((int) $userProgress->game * 10) + (int) $userProgress->round;
                        $theirProgress = ((int) $challengerProgress->game * 10) + (int) $challengerProgress->round;
                        if ($myProgress > $theirProgress) {
                            $askNewQuestion = true;
                        } else {
                            $theirQuestion = UserQuestion::where('user_progresses_id', $challengerProgress->id)->where('game', $userProgress->game)->where('round', $userProgress->round)->first();
                            $question = Question::find($theirQuestion->question_id);
                        }
                    }
                } else {
                    $askNewQuestion = true;
                }
                if ($askNewQuestion) {
                    if ($userProgress->challenge_id) {
                        $answeredQuestions = UserAnswer::whereIn('user_id', [$userProgress->challenge->user_1_id, $userProgress->challenge->user_2_id]);
                    } else {
                        $answeredQuestions = UserAnswer::where('user_id', Auth::user()->id);
                    }
                    $answeredQuestionIds = [];
                    if ($answeredQuestions) {
                        $answeredQuestionIds = $answeredQuestions->pluck('question_id');
                    }
                    $question = Question::where('difficulty', $this->progress[$userProgress->game][$userProgress->round])
                        ->where(function ($query) {
                            $query->where('all', true)
                                ->orwhere($this->getUserRegionTableName(), true);
                        })
                        ->whereNotIn('id', $answeredQuestionIds)
                        ->inRandomOrder()->first();
                    if (!$question) {
                        return null;
                    }
                }

                $userQuestion = new UserQuestion();
                $userQuestion->user_id = Auth::user()->id;
                $userQuestion->question_id = $question->id;
                $userQuestion->game = $userProgress->game;
                $userQuestion->round = $userProgress->round;
                $userQuestion->user_progresses_id = $userProgress->id;
                $userQuestion->save();

                $userProgress->question_id = $question->id;
                $userProgress->save();
                return $question;
            }
        }
    }

    public function getAllTransactions()
    {
        $answers = UserAnswer::with('user')->with('question')->with('userQuestion')->get();
        foreach ($answers as $answer) {
            if (isset($answer->userQuestion)) {
                $answer->speed = $answer->created_at->diffInSeconds($answer->userQuestion->created_at);
            } else {
                $answer->speed = null;
            }
        }
        return response()->json([
            'status' => 'success',
            'data' => $answers
        ], 200);
    }

    public function adminGetQuestion(Request $request, $spreadsheetIdentifier)
    {
        // var_dump($spreadsheetIdentifier);
        $question = Question::where('spreadsheet_identifier', $spreadsheetIdentifier)->with('userAnswer')->with('userQuestion')->get();
        return response()->json([
            'status' => 'success',
            'data' => $question
        ], 200);
    }

    public function getUserRank(Request $request)
    {
        $rankings = $this->rankUsers();

        $rank = array_search(Auth::user()->id, array_keys($rankings));

        return response()->json([
            'status' => 'success',
            'data' => [
                'rank' => $rank + 1 . "/" . count($rankings),
                'win_ratio' => $rankings[Auth::user()->id]['ratio'],
                'average' => $rankings[Auth::user()->id]['average_score'],
                // 'scores' => $rankings[Auth::user()->id]['scores']
            ]
        ], 200);
    }

    public function getUserAverage()
    {
        $rankings = $this->rankUsers();

        $rank = array_search(Auth::user()->id, array_keys($rankings));

        return $rankings[Auth::user()->id]['average_score'];
    }

    public function getAllUserRank(Request $request)
    {
        $rankings = $this->rankUsersWithAdditionalDetail();

        return response()->json([
            'status' => 'success',
            'data' => [
                'ranking' => array_values((array) $rankings)
            ]
        ], 200);
    }

    private function rankUsersWithAdditionalDetail()
    {
        $rankings = $this->rankUsers();

        $position = 1;
        $total = count($rankings);
        foreach ($rankings as $key => $rank) {
            $rankings[$key]['rank'] = $position . '/' . $total;
            $rankings[$key]['id'] = $key;
            $rankings[$key]['position'] = $position;
            unset($rankings[$key]['scores']);
            $position++;
        }

        return $rankings;
    }

    private function rankUsers()
    {
        $answers = UserAnswer::all();
        $scores = [];

        $users = User::all('id', 'firstname', 'lastname', 'email', 'region', 'avatar_filename', 'avatar_extension', 'notification_token');
        foreach ($users as $user) {
            $user['avatar'] = URL::to('storage/' . $user->avatar_filename . '.' . $user->avatar_extension);
            unset($user['avatar_filename']);
            unset($user['avatar_extension']);
            if ($user['notification_token'] == null) {
                $user['notification_token'] = '';
            }
            if (!isset($scores[$user->id])) {
                $scores[$user->id] = [];
                $scores[$user->id]['won'] = 0;
                $scores[$user->id]['lost'] = 0;
                $scores[$user->id]['ratio'] = 0;
                $scores[$user->id]['max_score'] = 0;
                $scores[$user->id]['average_score'] = 0;
                $scores[$user->id]['user'] = $user;
                $scores[$user->id]['scores'] = [];
            }
        }

        foreach ($answers as $answer) {
            if ($answer->answered_correctly) {
                $scores[$answer->user_id]['won'] = $scores[$answer->user_id]['won'] + 1;
            } else {
                $scores[$answer->user_id]['lost'] = $scores[$answer->user_id]['lost'] + 1;
            }
            if ($scores[$answer->user_id]['lost'] === 0) {
                $scores[$answer->user_id]['ratio'] = $scores[$answer->user_id]['won'];
            } else {
                $scores[$answer->user_id]['ratio'] = $scores[$answer->user_id]['won'] / $scores[$answer->user_id]['lost'];
            }
        }

        $progresses = UserProgress::all();
        foreach ($progresses as $progress) {
            $curProgress = 0;
            if ($progress->game == 0) {
                $curProgress = $progress->round;
            } else {
                $curProgress = (($progress->game - 1) * 10) + $progress->round;
            }
            if ($curProgress > $scores[$progress->user_id]['max_score']) {
                $scores[$progress->user_id]['max_score'] = $curProgress;
            }
            array_push($scores[$progress->user_id]['scores'], $curProgress);
        }

        foreach ($scores as $key => $score) {
            if (!empty($score['scores'])) {
                $scores[$key]['average_score'] = array_sum($score['scores']) / count($score['scores']);
            } else {
                $scores[$key]['average_score'] = 0;
            }

            // if(!empty($score[$key]) && !empty($score[$key]['scores']) && count($score[$key]['scores'] !== 0)) {
            //     $scores[$key]['average_score'] = array_sum($score['scores'])/count($score['scores']);
            // } else {
            // $scores[$key]['average_score'] = 0;
            // }
        }

        uasort($scores, function ($a, $b) {
            return $b['won'] <=> $a['won'];
        });
        return $scores;
    }

    private function getQuestionById(int $id)
    {
        return Question::find($id);
    }

    private function getCategoriesById(int $id)
    {
        return Category::find($id);
    }

    private function getUserRegionTableName()
    {
        $region = Auth::user()->region;
        if ($region == 'Australia') {
            return 'aust';
        }
        if ($region == 'America') {
            return 'usa';
        }
        if ($region == 'United Kingdom') {
            return 'uk';
        }
        if ($region == 'New Zealand') {
            return 'nz';
        }
    }

    private function balance()
    {
        $balance = Balance::where('user_id', Auth::user()->id)->first();
        if (!$balance) {
            $balance = new Balance();
            $balance->balance = 0;
            $balance->kels = 100;
            $balance->user_id = Auth::user()->id;
            $balance->save();
        }
        return $balance;
    }

    private function getUniqueTransactionReference()
    {
        while (true) {
            $reference = md5(uniqid(rand(), true));
            $transaction = Transaction::where('reference', $reference)->get();
            if ($transaction->isEmpty()) {
                return md5(uniqid(rand(), true));
            }
        }
    }

    private function isAllowedChallengeBet($bet)
    {
        return array_key_exists($bet, $this->allowedChallengeBets);
    }

    private function populateRuleset()
    {
        $this->progress[0][0] = self::EASY;
        $this->progress[0][1] = self::EASY;
        $this->progress[0][2] = self::EASY;
        $this->progress[0][3] = self::EASY;
        $this->progress[0][4] = self::EASY;
        $this->progress[0][5] = self::EASY;
        $this->progress[0][6] = self::EASY;
        $this->progress[0][7] = self::EASY;
        $this->progress[0][8] = self::MEDIUM;
        $this->progress[0][9] = self::MEDIUM;

        $this->progress[1][0] = self::EASY;
        $this->progress[1][1] = self::EASY;
        $this->progress[1][2] = self::EASY;
        $this->progress[1][3] = self::EASY;
        $this->progress[1][4] = self::EASY;
        $this->progress[1][5] = self::EASY;
        $this->progress[1][6] = self::EASY;
        $this->progress[1][7] = self::MEDIUM;
        $this->progress[1][8] = self::MEDIUM;
        $this->progress[1][9] = self::HARD;

        $this->progress[2][0] = self::EASY;
        $this->progress[2][1] = self::EASY;
        $this->progress[2][2] = self::EASY;
        $this->progress[2][3] = self::EASY;
        $this->progress[2][4] = self::EASY;
        $this->progress[2][5] = self::EASY;
        $this->progress[2][6] = self::MEDIUM;
        $this->progress[2][7] = self::MEDIUM;
        $this->progress[2][8] = self::HARD;
        $this->progress[2][9] = self::HARD;

        $this->progress[3][0] = self::EASY;
        $this->progress[3][1] = self::EASY;
        $this->progress[3][2] = self::EASY;
        $this->progress[3][3] = self::EASY;
        $this->progress[3][4] = self::EASY;
        $this->progress[3][5] = self::MEDIUM;
        $this->progress[3][6] = self::MEDIUM;
        $this->progress[3][7] = self::HARD;
        $this->progress[3][8] = self::HARD;
        $this->progress[3][9] = self::HARD;

        $this->progress[4][0] = self::EASY;
        $this->progress[4][1] = self::EASY;
        $this->progress[4][2] = self::MEDIUM;
        $this->progress[4][3] = self::MEDIUM;
        $this->progress[4][4] = self::MEDIUM;
        $this->progress[4][5] = self::HARD;
        $this->progress[4][6] = self::HARD;
        $this->progress[4][7] = self::HARD;
        $this->progress[4][8] = self::HARD;
        $this->progress[4][9] = self::DIFFICULT;

        $this->progress[5][0] = self::EASY;
        $this->progress[5][1] = self::MEDIUM;
        $this->progress[5][2] = self::MEDIUM;
        $this->progress[5][3] = self::MEDIUM;
        $this->progress[5][4] = self::HARD;
        $this->progress[5][5] = self::HARD;
        $this->progress[5][6] = self::HARD;
        $this->progress[5][7] = self::HARD;
        $this->progress[5][8] = self::DIFFICULT;
        $this->progress[5][9] = self::DIFFICULT;

        $this->progress[6][0] = self::MEDIUM;
        $this->progress[6][1] = self::MEDIUM;
        $this->progress[6][2] = self::HARD;
        $this->progress[6][3] = self::HARD;
        $this->progress[6][4] = self::HARD;
        $this->progress[6][5] = self::HARD;
        $this->progress[6][6] = self::DIFFICULT;
        $this->progress[6][7] = self::DIFFICULT;
        $this->progress[6][8] = self::DIFFICULT;
        $this->progress[6][9] = self::VERY_DIFFICULT;

        $this->progress[7][0] = self::MEDIUM;
        $this->progress[7][1] = self::MEDIUM;
        $this->progress[7][2] = self::HARD;
        $this->progress[7][3] = self::HARD;
        $this->progress[7][4] = self::HARD;
        $this->progress[7][5] = self::HARD;
        $this->progress[7][6] = self::DIFFICULT;
        $this->progress[7][7] = self::DIFFICULT;
        $this->progress[7][8] = self::DIFFICULT;
        $this->progress[7][9] = self::VERY_DIFFICULT;

        $this->progress[8][0] = self::MEDIUM;
        $this->progress[8][1] = self::HARD;
        $this->progress[8][2] = self::HARD;
        $this->progress[8][3] = self::DIFFICULT;
        $this->progress[8][4] = self::DIFFICULT;
        $this->progress[8][5] = self::DIFFICULT;
        $this->progress[8][6] = self::DIFFICULT;
        $this->progress[8][7] = self::VERY_DIFFICULT;
        $this->progress[8][8] = self::VERY_DIFFICULT;
        $this->progress[8][9] = self::VERY_DIFFICULT;

        $this->progress[9][0] = self::DIFFICULT;
        $this->progress[9][1] = self::DIFFICULT;
        $this->progress[9][2] = self::DIFFICULT;
        $this->progress[9][3] = self::DIFFICULT;
        $this->progress[9][4] = self::DIFFICULT;
        $this->progress[9][5] = self::VERY_DIFFICULT;
        $this->progress[9][6] = self::VERY_DIFFICULT;
        $this->progress[9][7] = self::VERY_DIFFICULT;
        $this->progress[9][8] = self::VERY_DIFFICULT;
        $this->progress[9][9] = self::VERY_DIFFICULT;
    }

    private function populateWinnings()
    {
        $this->winnings[0] = 5;
        $this->winnings[1] = 10;
        $this->winnings[2] = 25;
        $this->winnings[3] = 50;
        $this->winnings[4] = 100;
        $this->winnings[5] = 200;
        $this->winnings[6] = 500;
        $this->winnings[7] = 1000;
        $this->winnings[8] = 1500;
        $this->winnings[9] = 2500;

        $this->kels[0] = 1;
        $this->kels[1] = 2;
        $this->kels[2] = 5;
        $this->kels[3] = 10;
        $this->kels[4] = 20;
        $this->kels[5] = 40;
        $this->kels[6] = 70;
        $this->kels[7] = 100;
        $this->kels[8] = 150;
        $this->kels[9] = 250;
    }

    private function populateAllowedChallengeBets()
    {
        $this->allowedChallengeBets[20] = true;
        $this->allowedChallengeBets[50] = true;
        $this->allowedChallengeBets[75] = true;
        $this->allowedChallengeBets[100] = true;
        $this->allowedChallengeBets[150] = true;
        $this->allowedChallengeBets[200] = true;
        $this->allowedChallengeBets[250] = true;
        $this->allowedChallengeBets[500] = true;
        $this->allowedChallengeBets[1000] = true;
        $this->allowedChallengeBets[1500] = true;
        $this->allowedChallengeBets[2000] = true;
        $this->allowedChallengeBets[2500] = true;
        $this->allowedChallengeBets[5000] = true;
    }
}
