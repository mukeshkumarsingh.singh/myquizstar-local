<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balance;
use App\Transaction;
use App\User;
use App\AppleIap;
use App\AppleReceipt;
use Auth;
use Exception;
use Laravel\Cashier\Cashier;

use function GuzzleHttp\json_decode;

class BankController extends Controller
{
    const CREDIT = 'CREDIT';
    const DEBIT = 'DEBIT';

    const KELS = 'kels';
    const CURRENCY = 'currency';

    public function registerStripe(Request $request)
    {
        try {
            $stripeCustomer = $request->user()->createAsStripeCustomer();
            return response()->json([
                'status' => 'success',
                'data' => $stripeCustomer
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function getStripePaymentOptions(Request $request)
    {
        $paymentMethods = $request->user()->paymentMethods();
        $defaultPaymentMethod = $request->user()->defaultPaymentMethod();
        $result = [];

        foreach ($paymentMethods as $payment) {
            $tmpMethod = new \stdClass();
            if (isset($defaultPaymentMethod) && $payment->id == $defaultPaymentMethod->id) {
                $tmpMethod->default = true;
            } else {
                $tmpMethod->default = false;
            }
            $tmpMethod->value = $payment->id;
            $tmpMethod->brand = $payment->card->brand;
            $tmpMethod->last4 = $payment->card->last4;
            $tmpMethod->text = ucfirst($payment->card->brand) . ' ····' . $payment->card->last4 . ' (exp. ' . $payment->card->exp_month . '/' . $payment->card->exp_year . ')';
            array_push($result, $tmpMethod);
        }

        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    function getValidStripePaymentAmounts(Request $request)
    {
        $result = [
            [
                'value' => 599,
                'kels' => 59,
                'text' => '$5.99 + K59'
            ],
            [
                'value' => 999,
                'kels' => 99,
                'text' => '$9.99 + K99'
            ],
            [
                'value' => 1999,
                'kels' => 199,
                'text' => '$19.99 + K199'
            ],
            [
                'value' => 2999,
                'kels' => 299,
                'text' => '$29.99 + K299'
            ],

        ];
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    function getUserByStripeId(Request $request)
    {
        $stripeId = $request->stripe_id;
        $user = Cashier::findBillable($stripeId);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    function createSubscription(Request $request)
    {
        try {
            $user = $request->user();
            if (!$request->plan) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please provide a plan to subscribe to'
                ], 422);
            }
            if ($user->subscribedToPlan($request->plan)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'User is already subscribed to ' . $request->plan
                ], 422);
            }
            $response = $user->newSubscription('Statstar Cricket', 'price_1HGk2EIOM5jXF5evbZpKYB2b')->create($user->defaultPaymentMethod()->id);
            return response()->json([
                'status' => 'success',
                'data' => $response
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }

    function isSubscribed(Request $request)
    {
        try {
            $user = $request->user();
            if (!$request->plan) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please provide a plan to check'
                ], 422);
            }
            if ($user->subscribedToPlan($request->plan)) {
                return response()->json([
                    'status' => 'success',
                    'data' => true
                ], 200);
            } else {
                return response()->json([
                    'status' => 'success',
                    'data' => false
                ], 200);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }

    function creditKelsFromMyaak(Request $request)
    {
        $user = $request->user();
        if (!$request->hash) {
            return response()->json([
                'status' => 'error',
                'message' => 'Please provide a valid hash'
            ], 422);
        }
        if (!$request->amount) {
            return response()->json([
                'status' => 'error',
                'message' => 'Please provide a valid amount to credit'
            ], 422);
        }

        $http = new \GuzzleHttp\Client;
        try {
            $body = new \stdClass();
            $body->hash = $request->hash;
            $body->user_id = $user->id;
            $body->amount = floatval($request->amount);
            $response = $http->post(config('services.myaak.endpoint'), [
                'body' => json_encode($body)
            ]);
            // $decodedResponse = json_decode($response->getBody());
            if ($response->getStatusCode() == 200) {
                $balance = $this->balance();
                $amount = $request->amount;
                $balance->kels = $balance->kels + $amount;
                $balance->save();

                $transaction = new Transaction();
                $transaction->user_id = Auth::user()->id;
                $transaction->amount = 0;
                $transaction->balance = $balance->balance;
                $transaction->kels = $amount;
                $transaction->kels_balance = $balance->kels_balance + $amount;
                $transaction->type = self::CREDIT;
                $transaction->category = 'Credit';
                $transaction->description = 'Game credit from Myaak';
                $transaction->reference = $this->getUniqueTransactionReference();
                $transaction->save();
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'balance' => $balance->balance,
                        'kels' => $balance->kels
                    ]
                ], 200);
            }
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $err = new \stdClass();
            $err->message = json_decode($e->getResponse()->getBody(true));
            $err->status = 'error';
            return response()->json($err, $e->getCode());
        }
    }

    function createPaymentMethod(Request $request)
    {
        // $paymentMethod = $request->paymentMethod;
        $user = $request->user();
        $user->addPaymentMethod($request->payment_id);
        $user->updateDefaultPaymentMethod($request->payment_id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    function chargeStripe(Request $request)
    {
        try {
            //first create the user in stripe
            // check if we have a customer
            if (!Cashier::findBillable($request->user()->stripe_id)) {
                $stripeCustomer = $request->user()->createAsStripeCustomer();
            }
            //create the payment method
            // $request->user()->addPaymentMethod($request->payment_id);
            // $request->user()->updateDefaultPaymentMethod($request->payment_id);

            $payment = $request->user()->charge($request->amount, $request->payment_id);
            $balance = $this->balance();
            $amount = $request->amount;

            $balance->balance = $balance->balance + $amount;
            $balance->kels = $balance->kels + floor($amount / 10);
            try {
                $balance->save();
                $transaction = new Transaction();
                $transaction->user_id = Auth::user()->id;
                $transaction->amount = $amount;
                $transaction->balance = $balance->balance;
                $transaction->kels = floor($amount / 10);
                $transaction->kels_balance = $balance->kels;
                $transaction->type = self::CREDIT;
                $transaction->category = 'Top Up';
                $transaction->description = 'Stripe Payment Top Up';
                $transaction->reference = $this->getUniqueTransactionReference();
                $transaction->save();
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'balance' => $balance->balance,
                        'kels' => $balance->kels
                    ]
                ], 200);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unfortunately, we're unable to credit your account at this time. Please contact support."
                ], 422);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function getBalance(Request $request)
    {
        $balance = $this->balance();

        return response()->json([
            'status' => 'success',
            'data' => [
                'balance' => $balance->balance,
                'kels' => $balance->kels
            ]
        ], 200);
    }

    public function debit(Request $request)
    {
        $amount = $request->amount;
        if (!$amount || $amount < 0 || !is_numeric($amount)) {
            return response()->json([
                'status' => 'error',
                'message' => "Invalid debit amount."
            ], 400);
        }

        $type = $request->type;
        if (!$type || ($type != $this::KELS && $type != $this::CURRENCY)) {
            return response()->json([
                'status' => 'error',
                'message' => "Invalid debit type."
            ], 400);
        }

        $balance = $this->balance();
        if ($type == $this::KELS) {
            $balance->kels = $balance->kels - $amount;
        } else {
            $balance->balance = $balance->balance - $amount;
        }
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        if ($type == $this::CURRENCY) {
            $transaction->amount = $amount;
        } else {
            $transaction->amount = 0;
        }
        $transaction->balance = $balance->balance;
        if ($type == $this::KELS) {
            $transaction->kels = $amount;
        } else {
            $transaction->kels = 0;
        }
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::DEBIT;
        $transaction->category = 'Debit';
        $transaction->description = $request->reason;
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();

        return $this->getBalance($request);
    }

    public function credit(Request $request)
    {
        $amount = $request->amount;
        if (!$amount || $amount < 0 || !is_numeric($amount)) {
            return response()->json([
                'status' => 'error',
                'message' => "Invalid credit amount."
            ], 400);
        }
        // TODO: verify IAP via apple
        $http = new \GuzzleHttp\Client;
        $requestBody = json_encode(['receipt-data' => $request->receipt, 'password' => config('services.apple.shared_secret'), 'exclude-old-transactions' => true]);

        try {
            $response = $http->post(config('services.apple.prod_verify'), [
                'body' => $requestBody
            ]);
            $response = json_decode($response->getBody());
            if ($response->status == 21007) {
                // $response = null;
                $response = $http->post(config('services.apple.dev_verify'), [
                    'body' => $requestBody
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to verify app store payment (status: " . $response->status . "). Please contact support."
                ], 500);
            }
            $appleReceipt = json_decode($response->getBody())->receipt;
            if (count($appleReceipt->in_app) < 1) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to verify app store payment (no payment information found). Please contact support."
                ], 500);
            }

            if ($appleReceipt->bundle_id !==  'com.myquizstar.app') {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to verify app store payment (unknown application). Please contact support."
                ], 400);
            }

            // check if we have already processed this payment
            // check the trans id matches
            $transactionId = $request->transaction_id;
            $newTransaction = false;
            // $transactionMatch = false;
            foreach ($appleReceipt->in_app as $iap) {
                $tmpIap = AppleIap::where('transaction_id', $iap->transaction_id)->get();
                if ($tmpIap->isEmpty()) {
                    $newTransaction = true;
                    break;
                }
                // if($transactionId == $iap->transaction_id) {
                //     $transactionMatch = true;
                // }
            }

            if (!$newTransaction) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to verify app store payment (payment already processed). Please contact support."
                ], 400);
            }

            $newReceipt = AppleReceipt::create();
            $newReceipt->receipt_type = $appleReceipt->receipt_type;
            $newReceipt->adam_id = $appleReceipt->adam_id;
            $newReceipt->app_item_id = $appleReceipt->app_item_id;
            $newReceipt->bundle_id = $appleReceipt->bundle_id;
            $newReceipt->application_version = $appleReceipt->application_version;
            $newReceipt->download_id = $appleReceipt->download_id;
            $newReceipt->version_external_identifier = $appleReceipt->version_external_identifier;
            $newReceipt->receipt_creation_date = \DateTime::createFromFormat("Y-m-d H:i:s T", $appleReceipt->receipt_creation_date);
            $newReceipt->request_date = \DateTime::createFromFormat("Y-m-d H:i:s T", $appleReceipt->request_date);
            $newReceipt->original_purchase_date = \DateTime::createFromFormat("Y-m-d H:i:s T", $appleReceipt->original_purchase_date);
            $newReceipt->original_application_version = $appleReceipt->original_application_version;
            $newReceipt->save();

            foreach ($appleReceipt->in_app as $iap) {
                $tmpIap = AppleIap::where('transaction_id', $iap->transaction_id)->get();
                if ($tmpIap->isEmpty()) {
                    //only add the first one
                    $newIap = AppleIap::create();
                    $newIap->applereceipt_id = $newReceipt->id;
                    $newIap->quantity = $iap->quantity;
                    $newIap->product_id = $iap->product_id;
                    $newIap->transaction_id = $iap->transaction_id;
                    $newIap->original_transaction_id = $iap->original_transaction_id;
                    $newIap->purchase_date = \DateTime::createFromFormat("Y-m-d H:i:s T", $iap->purchase_date);
                    $newIap->original_purchase_date = \DateTime::createFromFormat("Y-m-d H:i:s T", $iap->original_purchase_date);
                    $newIap->is_trial_period = $iap->is_trial_period;
                    $newIap->save();
                    break;
                }
            }
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            // dd($e);
            return response()->json([
                'status' => 'error',
                'message' => "Unable to verify app store payment. Please contact support."
            ], 500);
        }

        $balance = $this->balance();
        $balance->balance = $balance->balance + $amount;
        $balance->kels = $balance->kels + floor($amount / 10);
        try {
            $balance->save();
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = $amount;
            $transaction->balance = $balance->balance;
            $transaction->kels = floor($amount / 10);
            $transaction->kels_balance = $balance->kels;
            $transaction->type = self::CREDIT;
            $transaction->category = 'Top Up';
            $transaction->description = 'Apple Payment Top Up';
            $transaction->reference = $this->getUniqueTransactionReference();
            $transaction->save();
            return response()->json([
                'status' => 'success',
                'data' => [
                    'balance' => $balance->balance,
                    'kels' => $balance->kels
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => "Unfortunately, we're unable to credit your account at this time. Please contact support."
            ], 400);
        }
    }

    public function getTransactions(Request $request)
    {
        $transactions = Transaction::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        if (!$transactions) {
            // no transactions
            return response()->json([
                'status' => 'success',
                'data' => []
            ], 200);
        }
        $result = array();
        foreach ($transactions as $transaction) {
            $data = array();

            $data['category'] = $transaction->category;
            $data['description'] = $transaction->description;
            $data['amount'] = $transaction->amount;
            $data['type'] = $transaction->type;
            $data['balance'] = $transaction->balance;
            $data['kels'] = $transaction->kels;
            $data['kels_balance'] = $transaction->kels_balance;
            $data['date'] = $transaction->created_at;
            $data['reference'] = $transaction->reference;

            array_push($result, $data);
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    public function getAllTransactions(Request $request)
    {
        $transactions = Transaction::with('user')->orderBy('id', 'DESC')->get();
        // $transactions = User::with('transactions')->get();
        if (!$transactions) {
            // no transactions
            return response()->json([
                'status' => 'success',
                'data' => []
            ], 200);
        }
        return response()->json([
            'status' => 'success',
            'data' => $transactions
        ], 200);
    }

    private function balance()
    {
        $balance = Balance::where('user_id', Auth::user()->id)->first();
        if (!$balance) {
            $balance = new Balance();
            $balance->balance = 0;
            $balance->kels = 100;
            $balance->user_id = Auth::user()->id;
            $balance->save();
        }
        return $balance;
    }

    private function getUniqueTransactionReference()
    {
        while (true) {
            $reference = md5(uniqid(rand(), true));
            $transaction = Transaction::where('reference', $reference)->get();
            if ($transaction->isEmpty()) {
                return $reference;
            }
        }
    }
}
