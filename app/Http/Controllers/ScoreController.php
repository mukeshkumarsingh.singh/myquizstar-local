<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserProgress;
use App\UserAnswer;
use Auth;

class ScoreController extends Controller
{
    const IN_PROGRESS = 'in_progress';
    const COMPLETE = 'complete';

    protected $scoringDollars = array();
    protected $scoringQuora = array();

    public function __construct()
    {
        $this->populateScoring();
    }

    public function getCurrentScore()
    {
        $userProgress = UserProgress::where('user_id', Auth::user()->id)->where('status', self::IN_PROGRESS)->first();
        if (!$userProgress) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No game active, start one using the start endpoint.'
            ], 200);
        }
        if ($userProgress->game == 0) {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100,
                    'win_dollars' => 0,
                    'win_quora' => 0,
                    'last_played' => $userProgress->updated_at
                ]
            ], 200);
        } else {
            $winDollars = 0.00;
            $winQuora = 0.0;
            for ($i = 0; $i < $userProgress->game; $i++) {
                $winDollars = $winDollars + $this->scoringDollars[$i];
                $winQuora = $winQuora + $this->scoringQuora[$i];
            }
            return response()->json([
                'status' => 'success',
                'data' => [
                    'game' => $userProgress->game + 1,
                    'round' => $userProgress->round + 1,
                    'status' => $userProgress->status,
                    'max_games' => 10,
                    'max_rounds' => 10,
                    'game_progress_percentage' => (($userProgress->game + 1) / 10) * 100,
                    'round_progress_percentage' => (($userProgress->round + 1) / 10) * 100,
                    'win_dollars' => round($winDollars, 2),
                    'win_quora' => round($winQuora, 1),
                    'last_played' => $userProgress->updated_at
                ]
            ], 200);
        }
    }

    public function getAllScores()
    {
        $plays = UserProgress::where('user_id', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();
        if (!$plays) {
            // no game active
            return response()->json([
                'status' => 'error',
                'message' => 'No games found or played, start one using the start endpoint.'
            ], 200);
        }
        $result = array();
        foreach ($plays as $play) {
            $data = array();
            if ($play->game == 0) {
                $data['game'] = $play->game + 1;
                $data['round'] = $play->round + 1;
                $data['status'] = $play->status;
                $data['max_games'] = 10;
                $data['max_rounds'] = 10;
                $data['game_progress_percentage'] = (($play->game + 1) / 10) * 100;
                $data['round_progress_percentage'] = (($play->round + 1) / 10) * 100;
                $data['win_dollars'] = 0;
                $data['win_quora'] = 0;
                $data['progress'] =
                    $data['last_played'] = $play->updated_at;
            } else {
                $winDollars = 0.00;
                $winQuora = 0.0;
                for ($i = 0; $i < $play->game; $i++) {
                    $winDollars = $winDollars + $this->scoringDollars[$i];
                    $winQuora = $winQuora + $this->scoringQuora[$i];
                }
                if ($play->status == self::COMPLETE && $play->game == 9 && $play->round == 9) {
                    //finished everything
                    $winDollars = $winDollars + $this->scoringDollars[9];
                    $winQuora = $winQuora + $this->scoringQuora[9];
                }
                $data['game'] = $play->game + 1;
                $data['round'] = $play->round + 1;
                $data['status'] = $play->status;
                $data['max_games'] = 10;
                $data['max_rounds'] = 10;
                $data['game_progress_percentage'] = (($play->game + 1) / 10) * 100;
                $data['round_progress_percentage'] = (($play->round + 1) / 10) * 100;
                $data['win_dollars'] = round($winDollars, 2);
                $data['win_quora'] = round($winQuora, 1);
                $data['last_played'] = $play->updated_at;
            }
            array_push($result, $data);
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }

    private function populateScoring()
    {
        $this->scoringDollars[0] = 0.05;
        $this->scoringDollars[1] = 0.10;
        $this->scoringDollars[2] = 0.25;
        $this->scoringDollars[3] = 0.50;
        $this->scoringDollars[4] = 1.00;
        $this->scoringDollars[5] = 2.00;
        $this->scoringDollars[6] = 5.00;
        $this->scoringDollars[7] = 10.00;
        $this->scoringDollars[8] = 15.00;
        $this->scoringDollars[9] = 25.00;

        $this->scoringQuora[0] = 0.5;
        $this->scoringQuora[1] = 1.0;
        $this->scoringQuora[2] = 2.5;
        $this->scoringQuora[3] = 5.0;
        $this->scoringQuora[4] = 10.0;
        $this->scoringQuora[5] = 20.0;
        $this->scoringQuora[6] = 50.0;
        $this->scoringQuora[7] = 100.0;
        $this->scoringQuora[8] = 150.0;
        $this->scoringQuora[9] = 250.0;
    }
}
