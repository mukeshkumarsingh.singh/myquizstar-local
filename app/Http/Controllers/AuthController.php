<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;
use App\Balance;
use App\Transaction;
use Auth;

class AuthController extends Controller
{
    const CREDIT = 'CREDIT';
    const DEBIT = 'DEBIT';

    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $err = new stdClass();
            if ($e->getCode() == 400) {
                $err->message = 'Please enter a valid username or password.';
            } else if ($e->getCode() == 401) {
                $err->message = 'Invalid credentials. Please try again.';
            } else {
                $err->message = 'Unable to log in.';
            }
            $err->status = 'error';
            return response()->json($err, $e->getCode());
        }
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'region' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            $firstKey = array_key_first($validator->errors()->messages());
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->messages()[$firstKey][0]
            ], 400);
        }

        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => '',
            'region' => $request->region
        ]);

        $user->createAsStripeCustomer();

        $loginRequest = new Request();
        $loginRequest->username = $request->email;
        $loginRequest->password = $request->password;

        $loginResponse = $this->login($loginRequest);


        $balance = $this->balance($user->id);
        $balance->balance = 0;
        $balance->kels = 100;
        $balance->save();

        $transaction = new Transaction();
        $transaction->user_id = $user->id;
        $transaction->amount = 0;
        $transaction->balance = $balance->balance;
        $transaction->kels = 200;
        $transaction->kels_balance = $balance->kels;
        $transaction->type = self::CREDIT;
        $transaction->category = 'Registration';
        $transaction->description = 'Registration bonus';
        $transaction->reference = $this->getUniqueTransactionReference();
        $transaction->save();

        return $loginResponse;
        // $response = new stdClass();
        // $response->data = $user;
        // return response()->json($response, 200);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out.', 200);
    }

    public function listRegions() {
        return response()->json([
            'status' => 'success',
            'data' => [
                'regions' => ['America', 'Australia', 'United Kingdom', 'New Zealand']
            ]
        ], 200);
    }

    private function balance($userId)
    {
        $balance = Balance::where('user_id', $userId)->first();
        if (!$balance) {
            $balance = new Balance();
            $balance->balance = 0;
            $balance->kels = 100;
            $balance->user_id = $userId;
            $balance->save();
        }
        return $balance;
    }

    private function getUniqueTransactionReference()
    {
        while (true) {
            $reference = md5(uniqid(rand(), true));
            $transaction = Transaction::where('reference', $reference)->get();
            if ($transaction->isEmpty()) {
                return md5(uniqid(rand(), true));
            }
        }
    }
}
