<?php

namespace App\Http\Controllers;

use App\Features;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function getFeatures(Request $request) {
        $features = Features::select(['id', 'feature', 'display_name', 'enabled'])->get();

        return response()->json([
            'status' => 'success',
            'data' => $features
        ], 200);
    }

    public function toggleFeature(Request $request) {
        $feature = Features::select(['id', 'feature', 'display_name', 'enabled'])->where('feature', $request->feature)->first();
        if(!$feature) {
            return response()->json([
                'status' => 'error',
                'message' => 'No such feature'
            ], 404);
        }
        $feature->enabled = !$feature->enabled;
        $feature->save();
        unset($feature->updated_at);
        return response()->json([
            'status' => 'success',
            'data' => $feature
        ], 200);
    }
}
