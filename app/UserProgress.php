<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProgress extends Model
{
    protected $fillable = [
        'user_id',
        'game',
        'round',
        'status',
        'question_id',
        'type',
        'challenge_id',
        'scheduled_k_o_games_id'
    ];


    public function category()
    {
        return $this->belongsTo('App\User');
        return $this->hasOne('App\Question');
    }

    public function challenge() {
        return $this->belongsTo('App\Challenge');
    }

    public function scheduledKOGame() {
        return $this->belongsTo('App\ScheduledKOGames');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
