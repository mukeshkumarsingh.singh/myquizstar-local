<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledKOGames extends Model
{
    protected $fillable = [
        'k_o_games_id',
        'scheduled_for',
        'completed',
        'winner_id',
    ];

    public function KOGames()
    {
        return $this->belongsTo('App\KOGame');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'winner_id');
    }
}
