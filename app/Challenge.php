<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    protected $fillable = [
        'bet_amount',
        'user_1_id',
        'user_2_id',
        'winner_id',
        'win_amount',
        'challenge_accepted'
    ];
}
