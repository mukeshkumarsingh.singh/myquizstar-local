<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledKOGamesQuestions extends Model
{
    protected $fillable = [
        'scheduled_k_o_games_id',
        'game',
        'round',
        'questions_id',
        'asked_at'
    ];

    public function ScheduledKOGame()
    {
        return $this->belongsTo('App\ScheduledKOGames');
    }

    public function Question()
    {
        return $this->belongsTo('App\Questions');
    }
}
