<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
        'user_id',
        'balance',
        'kels'
    ];


    public function category()
    {
        return $this->belongsTo('App\User');
    }
}
